#!/bin/sh

JAVA_OPTS="$JAVA_OPTS"

#JVM
#JAVA_OPTS="$JAVA_OPTS -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=51081"

# JVM debug
JAVA_OPTS="$JAVA_OPTS -Xms128M -Xmx1024M -XX:MaxPermSize=256M"

# Hibernate trace
JAVA_OPTS="$JAVA_OPTS -Dhibernate.showSql=false"
JAVA_OPTS="$JAVA_OPTS -Dhibernate.formatSql=true"

export JAVA_OPTS