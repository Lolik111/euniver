rem ---------------------------------------------------------------------------
rem Common block
rem ---------------------------------------------------------------------------

rem set JAVA_OPTS=%JAVA_OPTS% -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=58083

set JAVA_OPTS=%JAVA_OPTS% -Xms128M -Xmx1024M -XX:MaxPermSize=256M

rem Clear
set JAVA_OPTS=%JAVA_OPTS% -Djava.library.path=%SystemRoot%

rem set JAVA_OPTS=%JAVA_OPTS% 