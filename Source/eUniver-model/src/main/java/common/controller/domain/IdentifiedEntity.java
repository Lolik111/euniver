package common.controller.domain;

import java.io.Serializable;

/**
 * Created by Damir on 04.07.16.
 */
public interface IdentifiedEntity extends Serializable {

    Serializable getId();

}
