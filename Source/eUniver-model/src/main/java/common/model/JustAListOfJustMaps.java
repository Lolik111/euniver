package common.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by ПАХАРЬ on 21.11.2016.
 */
public class JustAListOfJustMaps {

    private List<JustAMap> objects = new ArrayList<>();

    public List<JustAMap> getObjects() {
        return objects;
    }

    public void setObjects(List<JustAMap> objects) {
        this.objects = objects;
    }
}
