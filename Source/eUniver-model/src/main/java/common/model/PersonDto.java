package common.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import university.domain.Person;
import university.domain.Student;

/**
 * Created by Ильнар on 02.08.2016.
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "inhtype")

@JsonSubTypes({
        @JsonSubTypes.Type(value = TeacherDto.class, name = "teacher"),
        @JsonSubTypes.Type(value = StudentDto.class, name = "student")})

@JsonIgnoreProperties(ignoreUnknown = true)
public class PersonDto extends BaseDto {

    private String firstName;

    private String secondName;

    private String patronymic;

    private String imageUrl;

    private Person.Type type;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Person.Type getType() {
        return type;
    }

    public void setType(Person.Type type) {
        this.type = type;
    }
}
