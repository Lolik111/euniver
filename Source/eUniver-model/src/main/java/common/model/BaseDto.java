package common.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Ильнар on 02.08.2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseDto {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
