package common.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubjectDto extends BaseDto {

    private String name;

    private GenericSubjectDto genericSubject;

    public GenericSubjectDto getGenericSubject() {
        return genericSubject;
    }

    public void setGenericSubject(GenericSubjectDto genericSubject) {
        this.genericSubject = genericSubject;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
