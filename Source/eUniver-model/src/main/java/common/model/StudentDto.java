package common.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
public class StudentDto extends PersonDto {
    /**
     * Если студент принадлежит к одному академическому потоку и группе в это поле заполняются эти значения в формате
     * "<номер курса> курс, <номер группы> группа"
     */
    private String details;

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
