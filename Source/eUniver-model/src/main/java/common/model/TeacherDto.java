package common.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Ильнар on 02.08.2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TeacherDto extends PersonDto {

    private Set<GenericSubjectDto> genericSubjects = new HashSet<>();

    public Set<GenericSubjectDto> getGenericSubjects() {
        return genericSubjects;
    }

    public void setGenericSubjects(Set<GenericSubjectDto> genericSubjects) {
        this.genericSubjects = genericSubjects;
    }
}
