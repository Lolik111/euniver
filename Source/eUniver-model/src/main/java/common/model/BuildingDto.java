package common.model;

/**
 * Created by Ильнар on 22.07.2016.
 */
public class BuildingDto extends BaseDto {

    private String name;

    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
