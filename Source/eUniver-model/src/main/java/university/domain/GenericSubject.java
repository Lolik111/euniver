package university.domain;

import common.controller.domain.LongIdEntity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ПАХАРЬ on 23.07.2016.
 */
@Entity
@Table(name = "generic_subject")
@AttributeOverride(name = "id", column = @Column(name = "generic_subject_id"))
public class GenericSubject extends LongIdEntity{

    @Basic
    @Column(name = "name", nullable = false, insertable = true, updatable = true)
    private String name;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity =  Teacher.class)
    @JoinTable(name = "teacher_generic_subject"
            , joinColumns = { @JoinColumn(name = "generic_subject_ref")}
            , inverseJoinColumns = { @JoinColumn(name = "teacher_ref") })
    private Set<Teacher> teachers = new HashSet<Teacher>();

    @OneToMany(fetch = FetchType.LAZY, targetEntity = Subject.class, mappedBy = "genericSubject")
    private Set<Subject> subjects = new HashSet<Subject>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(Set<Teacher> teachers) {
        this.teachers = teachers;
    }

    public Set<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(Set<Subject> subjects) {
        this.subjects = subjects;
    }
}
