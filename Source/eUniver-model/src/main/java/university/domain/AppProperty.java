package university.domain;

import common.controller.domain.IdentifiedEntity;
import common.controller.domain.LongIdEntity;

import javax.persistence.*;

/**
 * Created by ПАХАРЬ on 06.11.2016.
 */

@Entity
@Table(name = "app_property")
@AttributeOverride(name = "id", column = @Column(name = "app_property_id"))
public class AppProperty extends LongIdEntity {

    @Basic(optional = false)
    @Column(name = "name", nullable = false, insertable = true, updatable = true)
    private String name;

    @Basic(optional = true)
    @Column(name = "value", nullable = true, insertable = true, updatable = true)
    private String value;

    @Basic(optional = true)
    @Column(name = "description", nullable = true, insertable = true, updatable = true)
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void incrementValue(){
        Integer val = Integer.parseInt(value);
        val++;
        setValue(val.toString());
    }
}
