package university.domain;

import common.controller.domain.LongIdEntity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ПАХАРЬ on 23.07.2016.
 */
@Entity
@Table(name = "plan_element")
@AttributeOverride(name = "id", column = @Column(name = "plan_element_id"))
public class PlanElement extends LongIdEntity {

    @Enumerated(EnumType.STRING)
    @Column(name = "weekday", nullable = false, insertable = true, updatable = true)
    private Type weekday;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = ClassPlan.class)
    @JoinColumn(name = "class_plan_ref", nullable = false, insertable = true, updatable = true)
    private ClassPlan classPlan;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Room.class)
    @JoinColumn(name = "room_ref", nullable = true, insertable = true, updatable = true)
    private Room room;

    @Basic
    @Column(name = "number", nullable = false, insertable = true, updatable = true)
    private Integer number;

    public Type getWeekday() {
        return weekday;
    }

    public void setWeekday(Type weekday) {
        this.weekday = weekday;
    }

    public ClassPlan getClassPlan() {
        return classPlan;
    }

    public void setClassPlan(ClassPlan classPlan) {
        this.classPlan = classPlan;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public enum Type {
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY,
        SUNDAY
    }

}
