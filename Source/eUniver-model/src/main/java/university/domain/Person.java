package university.domain;

import common.controller.domain.LongIdEntity;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ПАХАРЬ on 23.07.2016.
 */
@Entity
@Table(name = "person")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name="type", discriminatorType = DiscriminatorType.STRING, length = 32)
@AttributeOverride(name = "id", column = @Column(name = "person_id"))
public abstract class Person extends LongIdEntity {

    @Basic(optional = false)
    @NotEmpty
    @Column(name = "first_name", nullable = false, insertable = true, updatable = true)
    private String firstName;

    @Basic(optional = false)
    @NotEmpty
    @Column(name = "second_name", nullable = false, insertable = true, updatable = true)
    private String secondName;

    @Basic
    @Column(name = "patronymic", nullable = true, insertable = true, updatable = true)
    private String patronymic;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false, insertable = false, updatable = false)
    private Type type;

    @Basic
    @Column(name = "image_url", nullable = true, insertable = true, updatable = true)
    private String imageUrl;

    @OneToMany(fetch = FetchType.LAZY, targetEntity = Account.class, mappedBy = "person")
    private Set<Account> accounts = new HashSet<>();

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Type getType() {
        return type;
    }

    protected void setType(Type type) {
        this.type = type;
    }

    public Set<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(Set<Account> accounts) {
        this.accounts = accounts;
    }

    public enum Type {
        TEACHER("TEACHER"),
        STUDENT("STUDENT");

        String type;

        Type(String type){
            this.type = type;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
