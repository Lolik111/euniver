package university.domain;

import common.controller.domain.LongIdEntity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ПАХАРЬ on 23.07.2016.
 */
@Entity
@Table(name = "subject")
@AttributeOverride(name = "id", column = @Column(name = "subject_id"))
public class Subject extends LongIdEntity {

    @Basic
    @Column(name = "name", nullable = false, insertable = true, updatable = true)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = GenericSubject.class)
    @JoinColumn(name = "generic_subject_ref")
    private GenericSubject genericSubject;

    @OneToMany(fetch = FetchType.LAZY, targetEntity = Course.class, mappedBy = "subject")
    private Set<Course> courses = new HashSet<Course>();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GenericSubject getGenericSubject() {
        return genericSubject;
    }

    public void setGenericSubject(GenericSubject genericSubject) {
        this.genericSubject = genericSubject;
    }

    public Set<Course> getCourses() {
        return courses;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }

}
