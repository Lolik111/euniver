package university.domain;

import common.controller.domain.LongIdEntity;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ПАХАРЬ on 23.07.2016.
 */
@Entity
@Table(name = "account")
@AttributeOverride(name = "id", column = @Column(name = "account_id"))
public class Account extends LongIdEntity {

    @Enumerated(EnumType.STRING)
    @Column( name = "type", nullable = false, insertable = true, updatable = true)
    private Type type;

    @Basic(optional = false)
    @NotEmpty
    @Length(min = 5, max = 20)
    @Column(name = "login", nullable = false, insertable = true, updatable = true)
    private String login;

    @Basic(optional = false)
    @NotEmpty
    @Column(name = "password", nullable = false, insertable = true, updatable = true)
    private String password;

    @Basic(optional = true)
    @Column(name = "login_time", nullable = true, insertable = true, updatable = true)
    private Date loginTime;

    @Basic(optional = true)
    @Column(name = "times_logged", nullable = true, insertable = true, updatable = true)
    private Integer timesLogged;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, targetEntity = Person.class)
    @JoinColumn(name = "person_ref", nullable = false)
    private Person person;

    @ManyToMany(cascade = CascadeType.ALL, targetEntity = Department.class)
    @JoinTable(name = "account_department",
            joinColumns = { @JoinColumn(name = "account_ref") }
            , inverseJoinColumns = { @JoinColumn(name = "department_ref") } )
    private Set<Department> faculties = new HashSet<Department>();

    @OneToMany(fetch = FetchType.LAZY, targetEntity = Class.class, mappedBy = "account")
    private Set<Class> classes = new HashSet<Class>();

    @OneToMany(fetch = FetchType.LAZY, targetEntity = ClassPlan.class, mappedBy = "account")
    private Set<ClassPlan> classPlans = new HashSet<ClassPlan>();

    public enum Type {
        ADMIN("ADMIN"),
        DEPARTMENT_ADMIN("DEPARTMENT_ADMIN"),
        TEACHER("TEACHER"),
        STUDENT("STUDENT");

        String type;

        Type(String type){
            this.type = type;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Set<Department> getFaculties() {
        return faculties;
    }

    public void setFaculties(Set<Department> faculties) {
        this.faculties = faculties;
    }

    public Set<Class> getClasses() {
        return classes;
    }

    public void setClasses(Set<Class> classes) {
        this.classes = classes;
    }

    public Set<ClassPlan> getClassPlans() {
        return classPlans;
    }

    public void setClassPlans(Set<ClassPlan> classPlans) {
        this.classPlans = classPlans;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public Integer getTimesLogged() {
        return timesLogged;
    }

    public void setTimesLogged(Integer timesLogged) {
        this.timesLogged = timesLogged;
    }

    public void setAccountId(Long id) {
        this.setId(id);
    }
}
