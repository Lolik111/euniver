package university.domain;

import common.controller.domain.LongIdEntity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ПАХАРЬ on 23.07.2016.
 */
@Entity
@Table(name = "course")
@AttributeOverride(name = "id", column = @Column(name = "course_id"))
public class Course extends LongIdEntity {

    @Basic
    @Column(name = "academic_year", nullable = false, insertable = true, updatable = true)
    private Integer academicYear;

    @Basic
    @Column(name = "semester", nullable = false, insertable = true, updatable = true)
    private Integer semester;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Subject.class)
    @JoinColumn(name = "subject_ref", nullable = false, insertable = true, updatable = true)
    private Subject subject;

    @OneToMany(fetch = FetchType.LAZY, targetEntity = ClassPlan.class, mappedBy = "course")
    private Set<ClassPlan> classPlans = new HashSet<ClassPlan>();

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Set<ClassPlan> getClassPlans() {
        return classPlans;
    }

    public void setClassPlans(Set<ClassPlan> classPlans) {
        this.classPlans = classPlans;
    }

    public Integer getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(Integer academicYear) {
        this.academicYear = academicYear;
    }

    public Integer getSemester() {
        return semester;
    }

    public void setSemester(Integer semester) {
        this.semester = semester;
    }
}
