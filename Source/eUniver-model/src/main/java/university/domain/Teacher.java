package university.domain;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by ПАХАРЬ on 25.07.2016.
 */

@Entity
@Table(name = "teacher")
@PrimaryKeyJoinColumn(name="teacher_id")
@DiscriminatorValue("TEACHER")
@AttributeOverride(name = "id", column = @Column(name = "teacher_id"))
public class Teacher extends Person {

    @OneToMany(fetch = FetchType.LAZY, targetEntity = ClassPlan.class, mappedBy = "teacher")
    Set<ClassPlan> classPlans = new HashSet<ClassPlan>();


    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity =  GenericSubject.class)
    @JoinTable(name = "teacher_generic_subject"
            , joinColumns = { @JoinColumn(name = "teacher_ref")}
            , inverseJoinColumns = { @JoinColumn(name = "generic_subject_ref") })
    private List<GenericSubject> genericSubjects = new ArrayList<>();

    public Set<ClassPlan> getClassPlans() {
        return classPlans;
    }

    public void setClassPlans(Set<ClassPlan> classPlans) {
        this.classPlans = classPlans;
    }

    public List<GenericSubject> getGenericSubjects() {
        return genericSubjects;
    }

    public void setGenericSubjects(List<GenericSubject> genericSubjects) {
        this.genericSubjects = genericSubjects;
    }
}
