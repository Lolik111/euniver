package university.domain;

import common.controller.domain.LongIdEntity;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ПАХАРЬ on 23.07.2016.
 */
@Entity
@Table(name = "class_plan")
@AttributeOverride(name = "id", column = @Column(name = "class_plan_id"))
public class ClassPlan extends LongIdEntity{

    @Basic
    @Column(name = "classes_count", nullable = false, insertable = true, updatable = true)
    private Integer classesCount;

    @Basic
    @Column(name = "period", nullable = false, insertable = true, updatable = true)
    private Integer period;

    @Basic
    @Column(name = "tag", nullable = false, insertable = true, updatable = true)
    private String tag;

    @Basic
    @Column(name = "start_date", nullable = false, insertable = true, updatable = true)
    private Date startDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false, insertable = true, updatable = true)
    private Type type;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "classPlan")
    private Set<PlanElement> planElements = new HashSet<PlanElement>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "visitorsGroup")
    private Set<StudentVisitor> studentVisitors = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "classPlan")
    private Set<Assignment> assignments = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Course.class)
    @JoinColumn(name = "course_ref", nullable = false, insertable = true, updatable = true)
    private Course course;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Teacher.class)
    @JoinColumn(name = "teacher_ref", nullable = false, insertable = true, updatable = true)
    private Teacher teacher;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Account.class)
    @JoinColumn(name = "account_ref", nullable = false, insertable = true, updatable = true)
    private Account account;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "classPlan")
    private Set<Class> classes = new HashSet<>();

    public Integer getClassesCount() {
        return classesCount;
    }

    public void setClassesCount(Integer classesCount) {
        this.classesCount = classesCount;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Set<PlanElement> getPlanElements() {
        return planElements;
    }

    public void setPlanElements(Set<PlanElement> planElements) {
        this.planElements = planElements;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Set<Class> getClasses() {
        return classes;
    }

    public void setClasses(Set<Class> classes) {
        this.classes = classes;
    }

    public Set<Assignment> getAssignments() {
        return assignments;
    }

    public void setAssignments(Set<Assignment> assignments) {
        this.assignments = assignments;
    }

    public Set<StudentVisitor> getStudentVisitors() {
        return studentVisitors;
    }

    public void setStudentVisitors(Set<StudentVisitor> studentVisitors) {
        this.studentVisitors = studentVisitors;
    }

    public enum Type {
        LECTURE,
        PRACTICE,
        ADDITIONAL
    }

}
