    package university.domain;

    import common.controller.domain.LongIdEntity;

    import javax.persistence.*;
    import java.sql.Timestamp;
    import java.util.HashSet;
    import java.util.Set;

    /**
     * Created by ПАХАРЬ on 23.07.2016.
     */
    @Entity
    @Table(name = "room")
    @AttributeOverride(name = "id", column = @Column(name = "room_id"))
    public class Room extends LongIdEntity {

        @Basic
        @Column(name = "number", nullable = false, insertable = true, updatable = true)
        private String number;

        @OneToMany(fetch = FetchType.LAZY, targetEntity = PlanElement.class, mappedBy = "room")
        private Set<PlanElement> planElements = new HashSet<PlanElement>();

        @OneToMany(fetch = FetchType.LAZY, targetEntity = Class.class, mappedBy = "room")
        private Set<Class> classes = new HashSet<Class>();

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "building_ref", nullable = false, insertable = true, updatable = true)
        private Building building;

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public Set<PlanElement> getPlanElements() {
            return planElements;
        }

        public void setPlanElements(Set<PlanElement> planElements) {
            this.planElements = planElements;
        }

        public Set<Class> getClasses() {
            return classes;
        }

        public void setClasses(Set<Class> classes) {
            this.classes = classes;
        }

        public Building getBuilding() {
            return building;
        }

        public void setBuilding(Building building) {
            this.building = building;
        }

    }
