package university.domain;

import common.controller.domain.LongIdEntity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ПАХАРЬ on 23.07.2016.
 */
@Entity
@Table(name = "academic_flow")
@AttributeOverride(name = "id", column = @Column(name = "academic_flow_id"))
public class AcademicFlow extends LongIdEntity {
    @Basic(optional = false)
    @Column(name = "enter_year", nullable = false, insertable = true, updatable = true)
    private Integer enterYear;

    @Basic
    @Column(name = "graduation_year", nullable = true, insertable = true, updatable = true)
    private Integer graduationYear;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false, insertable = true, updatable = true)
    private Type type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "department_ref", nullable = false)
    private Department department;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "academicFlow")
    private Set<AcademicGroup> academicGroups = new HashSet<AcademicGroup>();


    public Integer getEnterYear() {
        return enterYear;
    }

    public void setEnterYear(Integer enterYear) {
        this.enterYear = enterYear;
    }

    public Integer getGraduationYear() {
        return graduationYear;
    }

    public void setGraduationYear(Integer graduationYear) {
        this.graduationYear = graduationYear;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }


    public Set<AcademicGroup> getAcademicGroups() {
        return academicGroups;
    }

    public void setAcademicGroups(Set<AcademicGroup> academicGroups) {
        this.academicGroups = academicGroups;
    }

    public int getCurrentEducationYearNumber() {
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        boolean startOfEducationYear = calendar.get(Calendar.MONTH) >= 8;
//        если первый семестр, то добавляем к разнице единицу
        return currentYear - enterYear + (startOfEducationYear ? 1 : 0);
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public enum Type{
        BACHELORS,
        MASTERS,
        PHD
    }
}
