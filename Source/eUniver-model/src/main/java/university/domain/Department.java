package university.domain;

import common.controller.domain.LongIdEntity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ПАХАРЬ on 23.07.2016.
 */
@Entity
@Table(name = "department")
@AttributeOverride(name = "id", column = @Column(name = "department_id"))
public class Department extends LongIdEntity{

    @Basic
    @Column(name = "name", nullable = false, insertable = true, updatable = true)
    private String name;

    @OneToMany(fetch = FetchType.LAZY, targetEntity = AcademicFlow.class, mappedBy = "department")
    private Set<AcademicFlow> academicFlows = new HashSet<>();

    @ManyToMany(cascade = CascadeType.ALL, targetEntity = Account.class)
    @JoinTable(name = "account_department",
            joinColumns = { @JoinColumn(name = "department_ref")},
            inverseJoinColumns = { @JoinColumn(name = "account_ref") })
    private Set<Account> accounts = new HashSet<Account>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<AcademicFlow> getAcademicFlows() {
        return academicFlows;
    }

    public void setAcademicFlows(Set<AcademicFlow> academicFlows) {
        this.academicFlows = academicFlows;
    }

    public Set<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(Set<Account> accounts) {
        this.accounts = accounts;
    }
}
