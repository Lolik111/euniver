package university.domain;

import common.controller.domain.LongIdEntity;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

/**
 * Created by ПАХАРЬ on 23.07.2016.
 */
@Entity
@Table(name = "class")
@AttributeOverride(name = "id", column = @Column(name = "class_id"))
public class Class extends LongIdEntity {

    @Basic
    @Column(name = "date", nullable = false, insertable = true, updatable = true)
    private Date date;

    @Basic
    @Column(name = "number", nullable = false, insertable = true, updatable = true)
    private Integer number;

    @Basic
    @Column(name = "time", nullable = false, insertable = true, updatable = true)
    private Time time;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = Room.class)
    @JoinColumn(name = "room_ref", nullable = false, insertable = true, updatable = true)
    private Room room;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = ClassPlan.class)
    @JoinColumn(name = "class_plan_ref", nullable = false, insertable = true, updatable = true)
    private ClassPlan classPlan;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Account.class)
    @JoinColumn(name = "account_ref", nullable = false, insertable = true, updatable = true)
    private Account account;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }


    public ClassPlan getClassPlan() {
        return classPlan;
    }

    public void setClassPlan(ClassPlan classPlan) {
        this.classPlan = classPlan;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public enum Type {
        LECTURE,
        PRACTICE,
        ADDITIONAL
    }
}
