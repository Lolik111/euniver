package university.domain;

import common.controller.domain.LongIdEntity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ПАХАРЬ on 23.07.2016.
 */
@Entity
@Table(name = "academic_group")
@AttributeOverride(name = "id", column = @Column(name = "academic_group_id"))
public class AcademicGroup extends LongIdEntity {

    @Basic(optional = false)
    @Column(name = "name", nullable = false, insertable = true, updatable = true)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "academic_flow_ref")
    private AcademicFlow academicFlow;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "academicGroup")
    private Set<Student> students = new HashSet<Student>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    public AcademicFlow getAcademicFlow() {
        return academicFlow;
    }

    public void setAcademicFlow(AcademicFlow academicFlow) {
        this.academicFlow = academicFlow;
    }
}
