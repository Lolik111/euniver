package university.domain;

import common.controller.domain.LongIdEntity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ПАХАРЬ on 23.07.2016.
 */
@Entity
@Table(name = "visitors_group")
@AttributeOverride(name = "id", column = @Column(name = "visitors_group_id"))
public class VisitorsGroup extends LongIdEntity {

    @Basic
    @Column(name = "tag", nullable = true, insertable = true, updatable = true)
    private String tag;

    @OneToOne(fetch = FetchType.LAZY, targetEntity = ClassPlan.class, mappedBy = "visitorsGroup")
    private ClassPlan classPlan;


    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = Student.class)
    @JoinTable(name = "student_visitor"
            , joinColumns = { @JoinColumn(name = "visitors_group_ref") }
            , inverseJoinColumns = { @JoinColumn(name = "student_ref") })
    private Set<Student> students = new HashSet<Student>();


    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }


    public ClassPlan getClassPlan() {
        return classPlan;
    }

    public void setClassPlan(ClassPlan classPlan) {
        this.classPlan = classPlan;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

}
