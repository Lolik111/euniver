package university.domain;

import common.controller.domain.LongIdEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ПАХАРЬ on 02.11.2016.
 */

@Entity
@Table(name = "assignment",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"assignment_id", "class_plan_ref"})})
@AttributeOverride(name = "id", column = @Column(name = "assignment_id"))
public class Assignment extends LongIdEntity {

    @Basic(optional = false)
    @Column(name = "description", nullable = false, insertable = true, updatable = true)
    private String description;

    @Basic(optional = true)
    @Column(name = "materials_url", nullable = true, insertable = true, updatable = true)
    private String materialsUrl;

    @Basic(optional = false)
    @Column(name = "creation_date", nullable = false, insertable = true, updatable = true)
    private Date creationDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "class_plan_ref", nullable = false)
    private ClassPlan classPlan;

    @OneToMany(mappedBy = "assignment")
    private Set<AssignmentGrade> assignmentGrades = new HashSet<>();

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMaterialsUrl() {
        return materialsUrl;
    }

    public void setMaterialsUrl(String materialsUrl) {
        this.materialsUrl = materialsUrl;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public ClassPlan getClassPlan() {
        return classPlan;
    }

    public void setClassPlan(ClassPlan classPlan) {
        this.classPlan = classPlan;
    }

    public Set<AssignmentGrade> getAssignmentGrades() {
        return assignmentGrades;
    }

    public void setAssignmentGrades(Set<AssignmentGrade> assignmentGrades) {
        this.assignmentGrades = assignmentGrades;
    }
}
