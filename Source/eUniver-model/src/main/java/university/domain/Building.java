package university.domain;

import common.controller.domain.LongIdEntity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ПАХАРЬ on 23.07.2016.
 */
@Entity
@Table(name = "building")
@AttributeOverride(name = "id", column = @Column(name = "building_id"))
public class Building extends LongIdEntity {

    @Basic
    @Column(name = "name", nullable = false, insertable = true, updatable = true)
    private String name;

    @Basic
    @Column(name = "address", nullable = false, insertable = true, updatable = true)
    private String address;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = Room.class, mappedBy = "building")
    Set<Room> rooms = new HashSet<Room>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

        public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<Room> getRooms() {
        return rooms;
    }

    public void setRooms(Set<Room> rooms) {
        this.rooms = rooms;
    }
}
