package university.domain;

import university.domain.Account;
import university.domain.Person;
import university.domain.Student;

/**
 * Created by ПАХАРЬ on 17.11.2016.
 */


public class AccountDto extends Account {

    public void setAccountId(Long id) {
        this.setId(id);
    }

    public void setPersonRef(Long id){
        Person p = new Student();
        p.setId(
                id
        );
        this.setPerson(p);
    }
}
