package university.domain;

import common.controller.domain.LongIdEntity;

import javax.persistence.*;

/**
 * Created by ПАХАРЬ on 02.11.2016.
 */

@Entity
@Table(name = "assignment_grade")
@AttributeOverride(name = "id", column = @Column(name = "assignment_grade_id"))
public class AssignmentGrade extends LongIdEntity {
    @Basic
    @Column(name = "grade", nullable = true, insertable = true, updatable = true)
    private Integer grade;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns(value = {
            @JoinColumn(name = "assignment_ref", referencedColumnName = "assignment_id")
    }, foreignKey = @ForeignKey(name = "fk_assignment_grade_assignment"))
    private Assignment assignment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns(value = {
            @JoinColumn(name = "student_ref", referencedColumnName = "student_ref"),
            @JoinColumn(name = "visitors_group_ref", referencedColumnName = "visitors_group_ref")
    }, foreignKey = @ForeignKey(name = "fk_assignment_grade_student_visitor"))
    private StudentVisitor studentVisitor;


    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Assignment getAssignment() {
        return assignment;
    }

    public void setAssignment(Assignment assignment) {
        this.assignment = assignment;
    }

    public StudentVisitor getStudentVisitor() {
        return studentVisitor;
    }

    public void setStudentVisitor(StudentVisitor studentVisitor) {
        this.studentVisitor = studentVisitor;
    }
}
