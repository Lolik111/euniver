package university.domain;

import common.controller.domain.LongIdEntity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ПАХАРЬ on 02.11.2016.
 */


@Entity
@Table(name = "student_visitor")
@AttributeOverride(name = "id", column = @Column(name = "student_visitor_id"))
public class StudentVisitor extends LongIdEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "visitors_group_ref", nullable = false)
    private ClassPlan visitorsGroup;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_ref", nullable = false)
    private Student student;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "studentVisitor")
    private Set<AssignmentGrade> assignmentGrades = new HashSet<>();


    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public ClassPlan getVisitorsGroup() {
        return visitorsGroup;
    }

    public void setVisitorsGroup(ClassPlan visitorsGroup) {
        this.visitorsGroup = visitorsGroup;
    }
}
