package university.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ПАХАРЬ on 23.07.2016.
 */
@Entity
@Table(name = "student")
@PrimaryKeyJoinColumn(name="student_id")
@DiscriminatorValue("STUDENT")
@AttributeOverride(name = "id", column = @Column(name = "student_id"))
public class Student extends Person {

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity =  ClassPlan.class)
    @JoinTable(name = "student_visitor"
            , joinColumns = { @JoinColumn(name = "student_ref")}
            , inverseJoinColumns = { @JoinColumn(name = "visitors_group_ref") })
    private Set<ClassPlan> visitorses = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "academic_group_ref", nullable = false)
    private AcademicGroup academicGroup;


    public AcademicGroup getAcademicGroup() {
        return academicGroup;
    }

    public void setAcademicGroup(AcademicGroup academicGroup) {
        this.academicGroup = academicGroup;
    }

    public Set<ClassPlan> getVisitorses() {
        return visitorses;
    }

    public void setVisitorses(Set<ClassPlan> visitorses) {
        this.visitorses = visitorses;
    }
}
