package mobile.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import common.model.BaseDto;
import university.domain.Person;

/**
 * Created by Ильнар on 02.08.2016.
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = StudentUser.class, name = "StudentUser"),
        @JsonSubTypes.Type(value = TeacherUser.class, name = "TeacherUser")
})
public abstract class User extends BaseDto{

    private String name;

    private String secondName;

    private String lastName;

    private String imageUrl;

    private Person.Type type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Person.Type getType() {
        return type;
    }

    public void setType(Person.Type type) {
        this.type = type;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
