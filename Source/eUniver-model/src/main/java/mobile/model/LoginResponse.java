package mobile.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import common.model.PersonDto;

/**
 * Created by Ильнар on 02.08.2016.
 */
//@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginResponse {

    private String token;

    private PersonDto user;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


    public PersonDto getUser() {
        return user;
    }

    public void setUser(PersonDto user) {
        this.user = user;
    }
}
