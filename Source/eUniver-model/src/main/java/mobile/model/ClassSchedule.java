package mobile.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import common.model.ClassDto;
import common.model.StudentClassDto;

import java.util.*;

/**
 * Created by Ильнар on 02.08.2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClassSchedule {

    private List<? extends ClassDto> schedule = new ArrayList<>();

    private Long classDuration;

    public Long getClassDuration() {
        return classDuration;
    }

    public void setClassDuration(Long classDuration) {
        this.classDuration = classDuration;
    }

    public List<? extends ClassDto> getSchedule() {
        return schedule;
    }

    public void setSchedule(List<? extends ClassDto> schedule) {
        this.schedule = schedule;
    }
}
