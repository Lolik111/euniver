package mobile.model;

/**
 * Created by Ильнар on 02.08.2016.
 */
public class StudentUser extends User {

    /**
     * Если студент принадлежит к одному академическому потоку и группе в это поле заполняются эти значения в формате
     * "<номер курса> курс, <номер группы> группа"
     */
    private String details;

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
