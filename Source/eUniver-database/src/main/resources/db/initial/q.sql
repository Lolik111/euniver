/*
There are the most complicated queries we have in our app
Also you can find them in eUniver-service module: \Source\eUniver-service\src\main\java\common\controller\services
And the most simple queries + executing are in eUniver-dao module: \Source\eUniver-dao\src\main\java\university\common
*/


-- list of students with academic group
SELECT   student_id, first_name, second_name, patronymic, image_url, academic_group_id, name
FROM     student
         join person on student_id = person_id
         join academic_group on academic_group_ref = academic_group_id
ORDER BY second_name, first_name, patronymic;

-- student info, group
SELECT   student_id, first_name, second_name, patronymic, image_url, academic_group_id, name
FROM     (SELECT * FROM student WHERE student_id = 101) s
         join person on student_id = person_id
         join academic_group on academic_group_ref = academic_group_id;

-- 
-- student-class average grade
SELECT   student_id, class_plan_id, avg(grade)
FROM     student s
         join student_visitor on student_id = student_ref
         join class_plan on class_plan_id = visitors_group_ref
         left join assignment on class_plan_id = class_plan_ref
         left join assignment_grade ag on assignment_id = ag.assignment_ref and student_id = ag.student_ref
GROUP BY student_id, class_plan_id;

-- 
-- student courses with average grade
SELECT   student_id, course_id, sub.name, academic_year, avg(grade) avg_grade
FROM     student
         join student_visitor on student_id = student_ref
         join class_plan on class_plan_id = visitors_group_ref
         join course on course_id = course_ref
         join subject sub on subject_id = subject_ref
         left join assignment on class_plan_id = class_plan_ref
         left join assignment_grade ag on assignment_id = ag.assignment_ref and student_id = ag.student_ref
GROUP BY student_id, course_id, sub.name, academic_year;

-- student courses, average grade
SELECT   course_id, sub.name subject_name, academic_year, avg(grade) avg_grade
FROM     (SELECT * FROM student WHERE student_id = 101) s
         join student_visitor on student_id = student_ref
         join class_plan on class_plan_id = visitors_group_ref
         join course on course_id = course_ref
         join subject sub on subject_id = subject_ref
         left join assignment on class_plan_id = class_plan_ref
         left join assignment_grade ag on assignment_id = ag.assignment_ref and student_id = ag.student_ref
GROUP BY student_id, course_id, sub.name, academic_year;

-- list of groups with number of students
SELECT   academic_group_id, ag.name academic_group_name, department_id, dp.name department_name, count(student_id) student_count
FROM     academic_group ag
         join academic_flow on academic_flow_id = academic_flow_ref
         join department dp on department_id = department_ref
         left join student on academic_group_id = academic_group_ref
GROUP BY academic_group_id, ag.name, department_id, dp.name
ORDER BY department_name, academic_group_name;

-- group info
SELECT   academic_group_id, ag.name academic_group_name, department_id, dp.name department_name
FROM     (SELECT * FROM academic_group WHERE academic_group_id = 1) ag
         join academic_flow on academic_flow_id = academic_flow_ref
         join department dp on department_id = department_ref;

-- students from the group
SELECT   student_id, first_name, second_name, patronymic, image_url
FROM     student
         join person on student_id = person_id
WHERE    academic_group_ref = 1
ORDER BY second_name, first_name, patronymic;

-- rating of students for academic group
-- student info, group and average grade for the 
SELECT   student_id, first_name, second_name, patronymic, image_url, avg(avg_grade) grade
FROM     (SELECT   student_id, course_id, avg(grade) avg_grade
          FROM     (SELECT * FROM student WHERE academic_group_ref = 1) s
                   join student_visitor on student_id = student_ref
                   join class_plan on class_plan_id = visitors_group_ref
                   join course on course_id = course_ref
                   join subject sub on subject_id = subject_ref
                   left join assignment on class_plan_id = class_plan_ref
                   left join assignment_grade ag on assignment_id = ag.assignment_ref and student_id = ag.student_ref
          GROUP BY student_id, course_id) grades
          join person on student_id = person_id
GROUP BY student_id, first_name, second_name, patronymic, image_url
ORDER BY grade DESC NULLS LAST;

-- list of courses, number of teachers, number of students (only non-empty courses)
SELECT   course_id, name subject_name, academic_year, semester, teacher_count, student_count
FROM     course
         join subject on subject_id = subject_ref
         join (SELECT   course_ref, count(DISTINCT teacher_ref) teacher_count
               FROM     class_plan
               GROUP BY course_ref) t on course_id = t.course_ref
         join (SELECT   course_ref, count(DISTINCT student_id) student_count
               FROM     class_plan
                        join student_visitor on visitors_group_ref = class_plan_id
                        join student on student_id = student_ref
               GROUP BY course_ref) s on course_id = s.course_ref
ORDER BY academic_year DESC, name ASC;

-- rating of students for course
-- student info, group and average grade for the course
SELECT   student_id, first_name, second_name, patronymic, image_url, academic_group_id, name group_name, avg(grade) grade
FROM     student
         join student_visitor on student_id = student_ref
         join class_plan on class_plan_id = visitors_group_ref
         left join assignment on class_plan_id = class_plan_ref
         left join assignment_grade ag on assignment_id = ag.assignment_ref and student_id = ag.student_ref
         join person on student_id = person_id
         join academic_group on academic_group_ref = academic_group_id
WHERE    course_ref = 1
GROUP BY student_id, first_name, second_name, patronymic, image_url, academic_group_id, name
ORDER BY grade DESC NULLS LAST;

-- course info - subject, year, teachers count
SELECT   subject_id, name subject_name, academic_year, semester, count(DISTINCT teacher_ref) teacher_count
FROM     (SELECT * FROM course WHERE course_id = 1) c
         join subject on subject_id = subject_ref
         join class_plan on course_id = course_ref
         left join person on person_id = teacher_ref
GROUP BY subject_id, name, academic_year, semester;

-- list of course teachers and his/her class types (LECTURE, PRACTICE)
SELECT   person_id teacher_id, first_name, second_name, patronymic, image_url, string_agg(DISTINCT c.type, ', ') "types"
FROM     (SELECT * FROM class_plan WHERE course_ref = 1) c
         join person on person_id = teacher_ref
GROUP BY teacher_id, first_name, second_name, patronymic, image_url
ORDER BY second_name, first_name, patronymic;



-- teacherService
-- list of course students and his/her group
SELECT DISTINCT student_id, first_name, second_name, patronymic, image_url, academic_group_id, name group_name
FROM     (SELECT * FROM class_plan WHERE course_ref = 1) c
         join student_visitor on visitors_group_ref = class_plan_id
         join student on student_id = student_ref
         join person on person_id = student_id
         join academic_group on academic_group_ref = academic_group_id
ORDER BY second_name, first_name, patronymic;


-- teacherService
-- fetch Teacher's class plans - here for teacher with teacher_id=11
SELECT class_plan_id, t4.name as subject_name, academic_year, type, tag
FROM teacher t1
JOIN class_plan t2 on t2.teacher_ref = t1.teacher_id
JOIN course t3 on t3.course_id = t2.course_ref
JOIN subject t4 on t4.subject_id = t3.subject_ref
WHERE teacher_id = 11



-- teacherService
-- fetch average grade for each class plan for teacher_id = 11, not including such students' grades, who has average grade less than 70
SELECT class_plan_id, AVG(grade) as avg_grade
FROM class_plan t1
JOIN assignment t2 ON t1.class_plan_id = t2.class_plan_ref
LEFT JOIN assignment_grade t3 ON t2.assignment_id = t3.assignment_ref AND t2.class_plan_ref = t3.visitors_group_ref
WHERE NOT EXISTS
   (SELECT *
    FROM
        (--bad students
        SELECT t6.student_ref
        FROM
        (SELECT t5.student_ref, t3.class_plan_id, avg(grade) as avg_for_class_plan
        FROM
        class_plan t3
        JOIN assignment t4 ON t4.class_plan_ref = t3.class_plan_id
        JOIN assignment_grade t5 ON t5.assignment_ref = t4.assignment_id
        WHERE grade is not null
        GROUP by t5.student_ref, class_plan_id) t6
        GROUP BY student_ref
        HAVING AVG(t6.avg_for_class_plan) < 70) t7
    WHERE t3.student_ref = t7.student_ref
    ) AND t1.teacher_ref = 11
GROUP by class_plan_id


-- teacherService
-- fetch classes for a class_plan with id = 21
SELECT class_id, date, time, t4.number as class_number, room_id, t5.number as room_number, t6.name as building_name
FROM class_plan t1
JOIN class t4 ON t4.class_plan_ref = t1.class_plan_id
JOIN room t5 ON t5.room_id = t4.room_ref
JOIN building t6 ON t6.building_id = t5.building_ref
WHERE class_plan_id = 21


--teacherService
-- fetch assignments for class_plan with id = 21
SELECT assignment_id, creation_date, materials_url, description
FROM class_plan t1
JOIN assignment t2 ON t1.class_plan_id = t2.class_plan_ref
WHERE class_plan_id = 21


--teacherService
-- get count of needed (still not created) assignment_grades for assignment with id = 201
-- e.g. one more student enrolled our class_plan. If class_plan has assignment, that doesn't have appropriate
-- assignment_grade, this query returns number > 0. If it returns 0 - everything is nice, otherwise we execute following script
SELECT COUNT(*) as cc
FROM
  (SELECT t1.*
  FROM student_visitor t1
  LEFT JOIN assignment_grade t2 ON t1.student_ref = t2.student_ref AND t1.visitors_group_ref = t2.visitors_group_ref
  WHERE assignment_ref = 201
  EXCEPT
  SELECT t1.*
  FROM student_visitor t1
  JOIN assignment_grade t2 ON t1.student_ref = t2.student_ref AND t1.visitors_group_ref = t2.visitors_group_ref
  WHERE assignment_ref = 201) t4




--teacherService
-- add non existing assignments

INSERT INTO assignment_grade (visitors_group_ref, student_ref, assignment_ref, grade)
SELECT t4.visitors_group_ref, t4.student_ref, 201, NULL
FROM
  (SELECT t1.*
  FROM student_visitor t1
  LEFT JOIN assignment_grade t2 ON t1.student_ref = t2.student_ref AND t1.visitors_group_ref = t2.visitors_group_ref
  EXCEPT
  SELECT t1.*
  FROM student_visitor t1
  JOIN assignment_grade t2 ON t1.student_ref = t2.student_ref AND t1.visitors_group_ref = t2.visitors_group_ref) t4



-- teacherService
-- fetch students' grades by assignment with id = 201
SELECT (ROW_NUMBER() OVER (ORDER BY student_id ASC) - 1) as row_id, student_id, first_name, second_name, patronymic, image_url,
academic_group_id, t6.name as group_name, t7.grade
FROM assignment t1
JOIN class_plan t2 ON t1.class_plan_ref = t2.class_plan_id
JOIN student_visitor t3 ON t3.visitors_group_ref = t2.class_plan_id
JOIN student t4 ON t4.student_id = t3.student_ref
JOIN person t5 ON t5.person_id = t4.student_id
JOIN academic_group t6 ON t4.academic_group_ref = t6.academic_group_id
LEFT JOIN assignment_grade t7 ON t1.class_plan_ref = t7.visitors_group_ref AND t7.assignment_ref = t1.assignment_id AND t4.student_id = t7.student_ref
WHERE assignment_id = 201


--courseService
--get average grade for subject with id = 3
SELECT t1.subject_id, t1.name as subject_name, AVG(t5.grade) as avg_grade
FROM subject t1
JOIN course t2 ON t2.subject_ref = t1.subject_id
JOIN class_plan t3 ON t3.course_ref = t2.course_id
JOIN assignment t4 ON t4.class_plan_ref = t3.class_plan_id
JOIN assignment_grade t5 ON t5.assignment_ref = t4.assignment_id AND t5.visitors_group_ref = t4.class_plan_ref
WHERE subject_id = 3
GROUP BY subject_id, subject_name  LIMIT 1



--courseService
--get 3 subjects, that were choosed more frequently by students, who enrolled this subject (with id = 3)
SELECT t8.subject_id, t8.name as subject_name, count(*) as cc
FROM
(SELECT DISTINCT student_ref
FROM subject t1
JOIN course t2 ON t2.subject_ref = t1.subject_id
JOIN class_plan t3 ON t3.course_ref = t2.course_id
JOIN student_visitor t4 ON t4.visitors_group_ref = t3.class_plan_id
WHERE subject_id = 3
) ts
JOIN student_visitor t5 ON ts.student_ref = t5.student_ref
JOIN class_plan t6 ON t6.class_plan_id = t5.visitors_group_ref
JOIN course t7 ON t7.course_id = t6.course_ref
JOIN subject t8 ON t8.subject_id = t7.subject_ref
WHERE subject_id <> 3
GROUP BY subject_id, subject_name
ORDER BY cc DESC LIMIT 3


--scheduleService
--get all mobile data needed for student

SELECT t3.class_id, t3.date, t3.time, t3.number, t2.type, t4.room_id, t4.number as room_number, t9.building_id, t9.name as building_name, t6.subject_id, t6.name as subject_name, t5.academic_year, t5.semester, t10.generic_subject_id, t10.name,
t7.teacher_id, t8.first_name, t8.second_name
FROM student_visitor t1 JOIN class_plan t2 ON t1.visitors_group_ref = t2.class_plan_id
JOIN class t3 ON t3.class_plan_ref = t2.class_plan_id
JOIN room t4 ON t4.room_id = t3.room_ref
JOIN building t9 ON t4.building_ref = t9.building_id
JOIN course t5 ON t2.course_ref = t5.course_id
JOIN subject t6 ON t5.subject_ref = t6.subject_id
JOIN teacher t7 ON t2.teacher_ref = t7.teacher_id
JOIN person t8 ON t7.teacher_id = t8.person_id
JOIN generic_subject t10 ON t6.generic_subject_ref = t10.generic_subject_id
WHERE t3.date >= '2016-10-31' AND t3.date <= '2016-12-11' AND t1.student_ref = 430



--scheduleService
--get all mobile data needed for teacher

SELECT t3.class_id, t3.date, t3.time, t3.number, t2.type, t4.room_id, t4.number as room_number, t9.building_id, t9.name as building_name,
t6.subject_id, t6.name as subject_name, t5.academic_year, t5.semester, t10.generic_subject_id, t10.name
FROM teacher t1 JOIN class_plan t2 ON t1.teacher_id = t2.teacher_ref
JOIN class t3 ON t3.class_plan_ref = t2.class_plan_id
JOIN room t4 ON t4.room_id = t3.room_ref
JOIN building t9 ON t4.building_ref = t9.building_id
JOIN course t5 ON t2.course_ref = t5.course_id
JOIN subject t6 ON t5.subject_ref = t6.subject_id
JOIN generic_subject t10 ON t6.generic_subject_ref = t10.generic_subject_id
WHERE t3.date >= '2016-10-31' AND t3.date <= '2016-12-11' AND t1.teacher_id = 11