/*
-Here and below, in any entity or relationship we use surrogate not null, unique and autoincrement id attribute (can be PK),
 so we can identify any table with id, that very helps us to work with all these data in our web controllers, e.g.
 in dropdown lists, where we need identify selected value, in controller's mapping, etc.
*/

-- Changeset initial-schema.xml::1::RIGizatullin
-- Building, where lessons are going
CREATE TABLE BUILDING (
  BUILDING_ID BIGSERIAL NOT NULL,   -- surrogate PK
  NAME        VARCHAR   NOT NULL,   -- name, e.g. UI main building
  ADDRESS     VARCHAR   NOT NULL,   -- address, e.g. Example street, 38
  CONSTRAINT PK_BUILDING PRIMARY KEY (BUILDING_ID),
  UNIQUE (NAME)
);
--FD: id -> all other attributes, no partial dependencies, no transitive dependencies, so 3rd NF
COMMENT ON TABLE BUILDING IS 'Building, where lessons are going';

-- Changeset initial-schema.xml::2::RIGizatullin
-- a Person - can be teacher or student
CREATE TABLE PERSON (
  PERSON_ID   BIGSERIAL NOT NULL,   -- surrogate PK
  FIRST_NAME  VARCHAR   NOT NULL,   -- first name
  SECOND_NAME VARCHAR   NOT NULL,   -- last name
  PATRONYMIC  VARCHAR,    -- patronymic
  IMAGE_URL   VARCHAR,    -- url to avatar img
  TYPE        VARCHAR   NOT NULL,   -- enum - can be 'STUDENT' or 'TEACHER'
  CONSTRAINT PK_PERSON PRIMARY KEY (PERSON_ID)
);
--FD: id -> all other attributes, no partial dependencies, no transitive dependencies, so 3rd NF

COMMENT ON TABLE PERSON IS 'Person';

-- Changeset initial-schema.xml::3::RIGizatullin
-- Department/faculty, e.g. Computing technology
CREATE TABLE DEPARTMENT (
  DEPARTMENT_ID BIGSERIAL NOT NULL,   -- surrogate PK
  NAME          VARCHAR   NOT NULL,   -- department's name
  CONSTRAINT PK_DEPARTMENT PRIMARY KEY (DEPARTMENT_ID),
  UNIQUE (NAME)
);
--FD: id -> all other attributes, no partial dependencies, no transitive dependencies, so 3rd NF

COMMENT ON TABLE DEPARTMENT IS 'Universitys department';

-- Changeset initial-schema.xml::4::RIGizatullin
-- A common name for set of subjects of one type, e.g. Advanced math
CREATE TABLE GENERIC_SUBJECT (
  GENERIC_SUBJECT_ID BIGSERIAL NOT NULL,    -- surrogate PK
  NAME               VARCHAR   NOT NULL,    -- name
  CONSTRAINT PK_GENERIC_SUBJECT PRIMARY KEY (GENERIC_SUBJECT_ID),
  UNIQUE (NAME)
);
--FD: id -> all other attributes, no partial dependencies, no transitive dependencies, so 3rd NF
COMMENT ON TABLE GENERIC_SUBJECT IS 'A common name for set of subjects of one type';

-- Changeset initial-schema.xml::5::RIGizatullin
CREATE INDEX IX_PERSON_FIRST_NAME_SECOND_NAME_PATRONYMIC
  ON PERSON (FIRST_NAME, SECOND_NAME, PATRONYMIC);

-- Changeset initial-schema.xml::6::RIGizatullin
CREATE INDEX IX_PERSON_SECOND_NAME_FIRST_NAME_PATRONYMIC
  ON PERSON (SECOND_NAME, FIRST_NAME, PATRONYMIC);


-- Changeset initial-schema.xml::7::RIGizatullin
-- Student, inherited from person
CREATE TABLE STUDENT (
  STUDENT_ID BIGSERIAL NOT NULL,  -- surrogate PK
  CONSTRAINT PK_STUDENT PRIMARY KEY (STUDENT_ID),
  CONSTRAINT FK_STUDENT_PERSON FOREIGN KEY (STUDENT_ID) REFERENCES PERSON
);
--FD: id -> all other attributes, no partial dependencies, no transitive dependencies, so 3rd NF

COMMENT ON TABLE STUDENT IS 'Student';

-- Changeset initial-schema.xml::8::RIGizatullin
-- Teacher, inherited from student
CREATE TABLE TEACHER (
  TEACHER_ID BIGSERIAL NOT NULL,  -- surrogate PK
  CONSTRAINT PK_TEACHER PRIMARY KEY (TEACHER_ID),
  CONSTRAINT FK_TEACHER_PERSON FOREIGN KEY (TEACHER_ID) REFERENCES PERSON
);
--FD: id -> all other attributes, no partial dependencies, no transitive dependencies, so 3rd NF

COMMENT ON TABLE TEACHER IS 'Teacher';

-- Changeset initial-schema.xml::9::RIGizatullin
-- Room/auditorium, where lesson is going
CREATE TABLE ROOM (
  ROOM_ID      BIGSERIAL NOT NULL,  -- surrogate PK
  NUMBER       VARCHAR   NOT NULL,  -- number of room
  BUILDING_REF BIGINT    NOT NULL,  -- reference to a building, many to one, total participation
  CONSTRAINT PK_ROOM PRIMARY KEY (ROOM_ID),
  CONSTRAINT FK_ROOM_BULIDING FOREIGN KEY (BUILDING_REF) REFERENCES BUILDING
);
--FD: id -> all other attributes, no partial dependencies, no transitive dependencies, so 3rd NF
COMMENT ON TABLE ROOM IS 'Room, where lesson is going';

-- Changeset initial-schema.xml::10::RIGizatullin
-- Subject, e.g. DSA/DMD
CREATE TABLE SUBJECT (
  SUBJECT_ID          BIGSERIAL NOT NULL,   -- surrogate PK
  NAME                VARCHAR   NOT NULL,   -- subject name
  DESCRIPTION         VARCHAR,              -- some information about subject (syllabus)
  GENERIC_SUBJECT_REF BIGINT    NOT NULL,   -- reference to a generic subject, many to one, total participation
  CONSTRAINT PK_SUBJECT PRIMARY KEY (SUBJECT_ID),
  CONSTRAINT FK_TEACHER_GENERIC_SUBJECT FOREIGN KEY (GENERIC_SUBJECT_REF) REFERENCES GENERIC_SUBJECT
);
--FD: id -> all other attributes, no partial dependencies, no transitive dependencies, so 3rd NF

COMMENT ON TABLE SUBJECT IS 'Subject';

-- Changeset initial-schema.xml::11::RIGizatullin
-- Amount of groups of students, who entered at the one time on the same program, e.g. MS1 or BS3
CREATE TABLE ACADEMIC_FLOW (
  ACADEMIC_FLOW_ID BIGSERIAL NOT NULL,  -- surrogate PK
  ENTER_YEAR       INT       NOT NULL,  -- enter year
  GRADUATION_YEAR  INT,                 -- graduation year
  TYPE             VARCHAR   NOT NULL,  -- type of flow, enum, can be 'BACHELORS' or 'MASTERS' or 'PHD'
  DEPARTMENT_REF   BIGINT    NOT NULL,  -- reference to a department, many to one, total participation
  CONSTRAINT PK_ACADEMIC_FLOW PRIMARY KEY (ACADEMIC_FLOW_ID),
  CONSTRAINT FK_ACADEMIC_FLOW_DEPARTMENT FOREIGN KEY (DEPARTMENT_REF) REFERENCES DEPARTMENT
);
--FD: id -> all other attributes, no partial dependencies, no transitive dependencies, so 3rd NF
COMMENT ON TABLE ACADEMIC_FLOW IS 'Academic flow';

-- Changeset initial-schema.xml::12::RIGizatullin
-- Group of students, e.g. MS1-1
CREATE TABLE ACADEMIC_GROUP (
  ACADEMIC_GROUP_ID BIGSERIAL NOT NULL,   -- surrogate PK
  NAME              VARCHAR   NOT NULL,   -- name of group, e.g. MS1-1
  ACADEMIC_FLOW_REF BIGINT    NOT NULL,   -- reference to academic flow, many to one, total participation
  CONSTRAINT PK_ACADEMIC_GROUP PRIMARY KEY (ACADEMIC_GROUP_ID),
  CONSTRAINT FK_ACADEMIC_GROUP_ACADEMIC_FLOW FOREIGN KEY (ACADEMIC_FLOW_REF) REFERENCES ACADEMIC_FLOW
);
--FD: id -> all other attributes, no partial dependencies, no transitive dependencies, so 3rd NF

COMMENT ON TABLE ACADEMIC_GROUP IS 'Real academic group';

-- Changeset initial-schema.xml::13::RIGizatullin
-- User's account
CREATE TABLE ACCOUNT (
  ACCOUNT_ID   BIGSERIAL NOT NULL,  --surrogate PK,
  TYPE         VARCHAR   NOT NULL,  --account role, enum, can be 'ADMIN', 'DEPARTMENT_ADMIN', 'TEACHER', 'STUDENT'
  LOGIN        VARCHAR   NOT NULL,  --user's name to log in
  PASSWORD     VARCHAR   NOT NULL,  --user's password with salt
  LOGIN_TIME   TIMESTAMP WITH TIME ZONE,  -- time, when user tried to log in last time
  TIMES_LOGGED INT,                 -- how much times user tried to log in in last 10 minutes from login_time
  PERSON_REF   BIGINT    NOT NULL,  --reference to person, many-to-one, total participation
  CONSTRAINT PK_ACCOUNT PRIMARY KEY (ACCOUNT_ID),   --We decided to choose id instead of login as PK, cause there is no much difference in perfomance/normalization,
  --  but some tables references account, so we don't want store varchar as FK there, all PK/FK in our base is bigint
  CONSTRAINT FK_ACCOUNT_PERSON FOREIGN KEY (PERSON_REF) REFERENCES PERSON,
  UNIQUE (LOGIN)  -- make login unique (and indexed below) to provide necessary perfomance
);
--FD: id <-> login -> all other attributes, no partial dependencies, but transitive dependencies, so 2nd NF

COMMENT ON TABLE ACCOUNT IS 'Users account';

-- Changeset initial-schema.xml::14::RIGizatullin
CREATE INDEX IX_ACCOUNT_LOGIN
  ON ACCOUNT (LOGIN);

--adding reference from student to academic group, many to one, total participation
ALTER TABLE STUDENT
  ADD ACADEMIC_GROUP_REF BIGINT NOT NULL;

ALTER TABLE STUDENT
  ADD CONSTRAINT FK_STUDENT_ACADEMIC_GROUP FOREIGN KEY (ACADEMIC_GROUP_REF) REFERENCES ACADEMIC_GROUP (ACADEMIC_GROUP_ID);

-- Changeset initial-schema.xml::15::RIGizatullin
-- Table for relationship that shows that particular account manages particular department
-- account many-to-many department, partial participation
CREATE TABLE ACCOUNT_DEPARTMENT (
  ACCOUNT_DEPARTMENT_ID BIGSERIAL NOT NULL,   --surrogate id (not PK), used in web (shorter url/navigation)
  ACCOUNT_REF           BIGINT    NOT NULL,   --reference to account
  DEPARTMENT_REF        BIGINT    NOT NULL,   --reference to department
  CONSTRAINT FK_ACCOUNT_DEPARTMENT_DEPARTMENT FOREIGN KEY (DEPARTMENT_REF) REFERENCES DEPARTMENT,
  CONSTRAINT FK_ACCOUNT_DEPARTMENT_ACCOUNT FOREIGN KEY (ACCOUNT_REF) REFERENCES ACCOUNT,
  UNIQUE (ACCOUNT_DEPARTMENT_ID)
);
--FD: PK -> , no partial dependencies, no transitive dependencies, so 3rd NF

COMMENT ON TABLE ACCOUNT_DEPARTMENT IS 'Link table for department admins';

-- PK
ALTER TABLE ACCOUNT_DEPARTMENT
  ADD PRIMARY KEY (ACCOUNT_REF, DEPARTMENT_REF);

-- Changeset initial-schema.xml::16::RIGizatullin
-- Semi-annnual course of any subject, e.g. DMD, that taught in 2016 at first semester
CREATE TABLE COURSE (
  COURSE_ID     BIGSERIAL NOT NULL, -- surrogate PK
  ACADEMIC_YEAR INT       NOT NULL, -- year when course was taught
  SEMESTER      INT       NOT NULL, -- semester when course was taught
  SUBJECT_REF   BIGINT    NOT NULL, -- reference to subject, many to one, total participation
  CONSTRAINT PK_COURSE PRIMARY KEY (COURSE_ID),
  CONSTRAINT FK_COURSE_SUBJECT FOREIGN KEY (SUBJECT_REF) REFERENCES SUBJECT
);
--FD: id -> all other attributes, no partial dependencies, no transitive dependencies, so 3rd NF

COMMENT ON TABLE COURSE IS 'Semi-annnual course of any subject';

-- Changeset initial-schema.xml::17::RIGizatullin
-- Relationship teacher many-to-many generic subject.
-- It shows teacher's main occupation (!), teacher still can teach class plan of another subject
--e.g. Klimchick's generic subjects: robotics, but he teaches DSA lecture 2016/1, which generic subject is DSA

CREATE TABLE TEACHER_GENERIC_SUBJECT (
  TEACHER_GENERIC_SUBJECT_ID BIGSERIAL NOT NULL,  --surrogate not null unique id (not PK)
  GENERIC_SUBJECT_REF        BIGINT    NOT NULL,  -- reference to generic subject
  TEACHER_REF                BIGINT    NOT NULL,  -- reference to teacher
  CONSTRAINT FK_TEACHER_GENERIC_SUBJECT_TEACHER FOREIGN KEY (TEACHER_REF) REFERENCES TEACHER,
  CONSTRAINT FK_TEACHER_GENERIC_SUBJECT_GENERIC_SUBJECT FOREIGN KEY (GENERIC_SUBJECT_REF) REFERENCES GENERIC_SUBJECT,
  UNIQUE (TEACHER_GENERIC_SUBJECT_ID)
);
--FD: PK -> id, no partial dependencies, no transitive dependencies, so 3rd NF

COMMENT ON TABLE TEACHER_GENERIC_SUBJECT IS 'Link between Teacher and Generic subject';
--PK
ALTER TABLE TEACHER_GENERIC_SUBJECT
  ADD PRIMARY KEY (GENERIC_SUBJECT_REF, TEACHER_REF);

-- Changeset initial-schema.xml::18::RIGizatullin
-- Relationship student many-to-many class_plan. It shows student's enrollment in difference class_plans
CREATE TABLE STUDENT_VISITOR (
  STUDENT_VISITOR_ID BIGSERIAL NOT NULL,  --surrogate id (not PK)
  VISITORS_GROUP_REF BIGINT    NOT NULL,  --reference to class_plan
  STUDENT_REF        BIGINT    NOT NULL,  --reference to student
  CONSTRAINT FK_STUDENT_VISITOR_STUDENT FOREIGN KEY (STUDENT_REF) REFERENCES STUDENT,
  UNIQUE (STUDENT_VISITOR_ID)
);
--FD: PK -> id, no partial dependencies, no transitive dependencies, so 3rd NF

COMMENT ON TABLE STUDENT_VISITOR IS 'Link between visitorsGroup and student';
--PK
ALTER TABLE STUDENT_VISITOR
  ADD PRIMARY KEY (STUDENT_REF, VISITORS_GROUP_REF);

--Changeset initial-schema.xml::19::RIGizatullin
--Teacher academic plan of particular course for certain set of students.
--e.g. DSA 2016/1 practice for MS1-1
CREATE TABLE CLASS_PLAN (
  CLASS_PLAN_ID BIGSERIAL NOT NULL, --surrogate id PK
  CLASSES_COUNT INT       NOT NULL, --number of classes
  PERIOD        INT       NOT NULL, --period, 1 means every week, 2 means one by one
  START_DATE    DATE      NOT NULL, --start_date, e.g. 15 August
  TYPE          VARCHAR   NOT NULL, --enum, can be 'LECTURE' or 'PRACTICE'
  TAG           VARCHAR   NOT NULL, --tag of set of students, e.g. MS1-1 or E5
  COURSE_REF    BIGINT    NOT NULL, --reference to course, many to one, full participation
  TEACHER_REF   BIGINT    NOT NULL, --reference to teacher, many to one, full participation
  ACCOUNT_REF   BIGINT    NOT NULL, --reference to account who created this plan, many to one, full participation
  CONSTRAINT PK_CLASS_PLAN PRIMARY KEY (CLASS_PLAN_ID),
  CONSTRAINT FK_CLASS_PLAN_COURSE FOREIGN KEY (COURSE_REF) REFERENCES COURSE,
  CONSTRAINT FK_CLASS_PLAN_TEACHER FOREIGN KEY (TEACHER_REF) REFERENCES TEACHER,
  CONSTRAINT FK_CLASS_PLAN_ACCOUNT FOREIGN KEY (ACCOUNT_REF) REFERENCES ACCOUNT
);
--FD: id -> all other attributes, no partial dependencies, no transitive dependencies, so 3rd NF

ALTER TABLE STUDENT_VISITOR
  ADD CONSTRAINT FK_STUDENT_VISITOR_CLASS_PLAN FOREIGN KEY (VISITORS_GROUP_REF) REFERENCES CLASS_PLAN (CLASS_PLAN_ID);


COMMENT ON TABLE CLASS_PLAN IS 'Course studying plan';


-- Changeset initial-schema.xml::20::RIGizatullin
--Assignment on certain class plan, e.g. GA-1
CREATE TABLE ASSIGNMENT (
  ASSIGNMENT_ID  BIGSERIAL                NOT NULL, --surrogate id PK
  DESCRIPTION    VARCHAR                  NOT NULL, --description
  MATERIALS_URL  VARCHAR,                           --url of some attachment
  CREATION_DATE  TIMESTAMP WITH TIME ZONE NOT NULL, --creation date of assignment
  CLASS_PLAN_REF BIGINT                   NOT NULL, --reference to class_plan, many to one, full participation
  CONSTRAINT PK_ASSIGNMENT PRIMARY KEY (ASSIGNMENT_ID),
  CONSTRAINT FK_ASSIGNMENT_CLASS_PLAN FOREIGN KEY (CLASS_PLAN_REF) REFERENCES CLASS_PLAN
);
--FD: id -> all other attributes, no partial dependencies, no transitive dependencies, so 3rd NF

COMMENT ON TABLE ASSIGNMENT IS 'Assignment';

ALTER TABLE ASSIGNMENT
  ADD UNIQUE (ASSIGNMENT_ID, CLASS_PLAN_REF);

-- Changeset initial-schema.xml::21::RIGizatullin
--Aggregation with grade attribute. student_visitor (student, enrolled in particular class_plan) many-to-many assignment on the same class_plan
CREATE TABLE ASSIGNMENT_GRADE (
  ASSIGNMENT_GRADE_ID BIGSERIAL NOT NULL, --surrogate id (not PK)
  GRADE               INT,                --grade, can be from 0 to 100 (look for check constraint in changeset #28). NULL means not graded yet
  ASSIGNMENT_REF      BIGINT    NOT NULL, --assignment
  STUDENT_REF         BIGINT    NOT NULL, --student
  VISITORS_GROUP_REF  BIGINT    NOT NULL, --class_plan (remember, that each class plan has some students enrolled in)
  UNIQUE (ASSIGNMENT_GRADE_ID)
);
--FD: PK -> id, grade no partial dependencies, no transitive dependencies,
-- but assignment_ref -> visitors_group, so 3NF, but not 4NF

COMMENT ON TABLE ASSIGNMENT_GRADE IS 'Grade for an assignment for each student';
-- PK
ALTER TABLE ASSIGNMENT_GRADE
  ADD PRIMARY KEY (ASSIGNMENT_REF, STUDENT_REF, VISITORS_GROUP_REF);

-- Changeset initial-schema.xml::22::RIGizatullin

--reference from assignment_grade to assignment, many to one

ALTER TABLE ASSIGNMENT_GRADE
  ADD CONSTRAINT FK_ASSIGNMENT_GRADE_ASSIGNMENT FOREIGN KEY (ASSIGNMENT_REF, VISITORS_GROUP_REF) REFERENCES ASSIGNMENT (ASSIGNMENT_ID, CLASS_PLAN_REF);

--reference from assignment_grade to assignment, many to one
ALTER TABLE ASSIGNMENT_GRADE
  ADD CONSTRAINT FK_ASSIGNMENT_GRADE_STUDENT_VISITOR FOREIGN KEY (VISITORS_GROUP_REF, STUDENT_REF) REFERENCES STUDENT_VISITOR (VISITORS_GROUP_REF, STUDENT_REF);

-- Changeset initial-schema.xml::23::RIGizatullin
--One element of academic plan, using to generate classes, e.g. lesson at #3 on Monday on English 2016/1 for E5 in 303 room
CREATE TABLE PLAN_ELEMENT (
  PLAN_ELEMENT_ID BIGSERIAL NOT NULL, --surrogate PK
  WEEKDAY         VARCHAR   NOT NULL, --enum, can be 'SUNDAY', 'MONDAY', etc..
  CLASS_PLAN_REF  BIGINT    NOT NULL, --reference to class_plan
  ROOM_REF        BIGINT,             --reference to room, there lesson is going
  NUMBER          INT       NOT NULL, --number of lesson in a day, e.g. 1 means first lesson - at 9:00, 2 means second lesson - at 10:40, etc.
  CONSTRAINT PK_PLAN_ELEMENT PRIMARY KEY (PLAN_ELEMENT_ID),
  CONSTRAINT FK_PLAN_ELEMENT_ROOM FOREIGN KEY (ROOM_REF) REFERENCES ROOM,
  CONSTRAINT FK_PLAN_ELEMENT_CLASS_PLAN FOREIGN KEY (CLASS_PLAN_REF) REFERENCES CLASS_PLAN
);
--FD: id -> all other attributes, no partial dependencies, no transitive dependencies, so 3rd NF

COMMENT ON TABLE PLAN_ELEMENT IS 'An element of template for classes';

-- Changeset initial-schema.xml::24::RIGizatullin
-- One lesson
CREATE TABLE CLASS (
  CLASS_ID       BIGSERIAL NOT NULL,  --surrogate PK
  DATE           DATE      NOT NULL,  --date of lesson
  NUMBER         INT       NOT NULL,  --number of lesson (1 - 9:00, 2 - 10:40, etc)
  TIME           TIME      NOT NULL,  --time of lesson
  ROOM_REF       BIGINT,              --reference to room
  CLASS_PLAN_REF BIGINT    NOT NULL,  --reference to class_plan
  ACCOUNT_REF    BIGINT    NOT NULL,  --reference to account, which was used in creation of this class
  CONSTRAINT PK_CLASS PRIMARY KEY (CLASS_ID),
  CONSTRAINT FK_CLASS_CLASS_PLAN FOREIGN KEY (CLASS_PLAN_REF) REFERENCES CLASS_PLAN,
  CONSTRAINT FK_CLASS_ROOM FOREIGN KEY (ROOM_REF) REFERENCES ROOM,
  CONSTRAINT FK_CLASS_ACCOUNT FOREIGN KEY (ACCOUNT_REF) REFERENCES ACCOUNT
);
--FD: id -> all other attributes
--    id -> number -> time, so 2NF

COMMENT ON TABLE CLASS IS 'Class';

-- Changeset initial-schema.xml::25::RIGizatullin
CREATE UNIQUE INDEX UIX_CLASS_DATE_NUMBER_ROOM
  ON CLASS (DATE, NUMBER, ROOM_REF);

-- Changeset initial-schema.xml::26::RIGizatullin
--Table with different properties
CREATE TABLE APP_PROPERTY (
  APP_PROPERTY_ID BIGSERIAL NOT NULL UNIQUE, --surrogate id
  NAME            VARCHAR   NOT NULL,        --key of property
  VALUE           VARCHAR,                   --value of property
  DESCRIPTION     VARCHAR,                   --description of property
  CONSTRAINT PK_APP_PROPERTY PRIMARY KEY (NAME)
);
--NF: not an entity. But it is in 3ND, cause name -> other attributes
COMMENT ON TABLE APP_PROPERTY IS 'Key-value dictionary with variety of attributes';

-- Changeset initial-schema.xml::27::RIGizatullin
INSERT INTO APP_PROPERTY (NAME, VALUE, DESCRIPTION) VALUES ('log_in', '0', 'How much log in take place');

INSERT INTO APP_PROPERTY (NAME, VALUE, DESCRIPTION) VALUES ('s_log_in', '0', 'Successful logs in');


-- Changeset initial-schema.xml::28::AMValiullin
ALTER TABLE assignment_grade
  ADD CONSTRAINT grade CHECK (grade >= 0 AND grade <= 100);
