
CREATE TABLE public.academic_flow (
	academic_flow_id BIGINT NOT NULL DEFAULT nextval('academic_flow_academic_flow_id_seq'::regclass),
	enter_year INTEGER NOT NULL,
	graduation_year INTEGER NULL,
	"type" varchar NOT NULL,
	department_ref BIGINT NOT NULL,
	CONSTRAINT pk_academic_flow PRIMARY KEY (academic_flow_id),
	CONSTRAINT fk_academic_flow_department FOREIGN KEY (department_ref) REFERENCES public.department(department_id)
)
WITH (
	OIDS=FALSE
);

CREATE TABLE public.academic_group (
	academic_group_id BIGINT NOT NULL DEFAULT nextval('academic_group_academic_group_id_seq'::regclass),
	"name" varchar NOT NULL,
	academic_flow_ref BIGINT NOT NULL,
	CONSTRAINT pk_academic_group PRIMARY KEY (academic_group_id),
	CONSTRAINT fk_academic_group_academic_flow FOREIGN KEY (academic_flow_ref) REFERENCES public.academic_flow(academic_flow_id)
)
WITH (
	OIDS=FALSE
);

CREATE TABLE public.account (
	account_id BIGINT NOT NULL DEFAULT nextval('account_account_id_seq'::regclass),
	"type" varchar NOT NULL,
	login varchar NOT NULL,
	password varchar NOT NULL,
	person_ref BIGINT NOT NULL,
	login_time timestamptz NULL,
	times_logged INTEGER NULL,
	CONSTRAINT pk_account PRIMARY KEY (account_id),
	CONSTRAINT fk_account_person FOREIGN KEY (person_ref) REFERENCES public.person(person_id)
)
WITH (
	OIDS=FALSE
);
CREATE INDEX ix_account_login ON public.account (login);

CREATE TABLE public.account_department (
	account_department_id BIGINT NOT NULL DEFAULT nextval('account_department_account_department_id_seq'::regclass),
	account_ref BIGINT NOT NULL,
	department_ref BIGINT NOT NULL,
	CONSTRAINT pk_account_department PRIMARY KEY (account_department_id),
	CONSTRAINT fk_account_department_account FOREIGN KEY (account_ref) REFERENCES public.account(account_id),
	CONSTRAINT fk_account_department_department FOREIGN KEY (department_ref) REFERENCES public.department(department_id)
)
WITH (
	OIDS=FALSE
);

CREATE TABLE public.app_property (
	app_property_id BIGINT NOT NULL DEFAULT nextval('app_property_app_property_id_seq'::regclass),
	"name" varchar NOT NULL,
	value varchar NULL,
	description varchar NULL,
	CONSTRAINT pk_app_property PRIMARY KEY (app_property_id)
)
WITH (
	OIDS=FALSE
);

CREATE TABLE public."assignment" (
	assignment_id BIGINT NOT NULL DEFAULT nextval('assignment_assignment_id_seq'::regclass),
	description varchar NOT NULL,
	materials_url varchar NULL,
	creation_date timestamptz NOT NULL,
	class_plan_ref BIGINT NOT NULL,
	CONSTRAINT assignment_assignment_id_class_plan_ref_key UNIQUE (assignment_id,class_plan_ref),
	CONSTRAINT pk_assignment PRIMARY KEY (assignment_id),
	CONSTRAINT fk_assignment_class_plan FOREIGN KEY (class_plan_ref) REFERENCES public.class_plan(class_plan_id)
)
WITH (
	OIDS=FALSE
);

CREATE TABLE public.assignment_grade (
	assignment_grade_id BIGINT NOT NULL DEFAULT nextval('assignment_grade_assignment_grade_id_seq'::regclass),
	assignment_ref BIGINT NOT NULL,
	student_ref BIGINT NOT NULL,
	visitors_group_ref BIGINT NOT NULL,
	grade INTEGER NULL,
	CONSTRAINT grade CHECK((grade >= 0) AND (grade <= 100)),
	CONSTRAINT pk_assignment_grade PRIMARY KEY (assignment_grade_id),
	CONSTRAINT fk_assignment_grade_assignment FOREIGN KEY (assignment_ref,visitors_group_ref) REFERENCES public."assignment"(assignment_id,class_plan_ref),
	CONSTRAINT fk_assignment_grade_student_visitor FOREIGN KEY (visitors_group_ref,student_ref) REFERENCES public."student_visitor"(visitors_group_ref, student_ref)
)
WITH (
	OIDS=FALSE
);

CREATE TABLE public.building (
	building_id BIGINT NOT NULL DEFAULT nextval('building_building_id_seq'::regclass),
	"name" varchar NOT NULL,
	address varchar NOT NULL,
	CONSTRAINT pk_building PRIMARY KEY (building_id)
)
WITH (
	OIDS=FALSE
);

CREATE TABLE public.class (
	class_id BIGINT NOT NULL DEFAULT nextval('class_class_id_seq'::regclass),
	"date" date NOT NULL,
	"number" INTEGER NOT NULL,
	"time" time NOT NULL,
	room_ref BIGINT NULL,
	class_plan_ref BIGINT NOT NULL,
	account_ref BIGINT NOT NULL,
	CONSTRAINT pk_class PRIMARY KEY (class_id),
	CONSTRAINT fk_class_account FOREIGN KEY (account_ref) REFERENCES public.account(account_id),
	CONSTRAINT fk_class_class_plan FOREIGN KEY (class_plan_ref) REFERENCES public.class_plan(class_plan_id),
	CONSTRAINT fk_class_room FOREIGN KEY (room_ref) REFERENCES public.room(room_id)
)
WITH (
	OIDS=FALSE
);
CREATE UNIQUE INDEX uix_class_date_number_room ON public.class (date,number,room_ref);

CREATE TABLE public.class_plan (
	class_plan_id BIGINT NOT NULL DEFAULT nextval('class_plan_class_plan_id_seq'::regclass),
	classes_count INTEGER NOT NULL,
	period INTEGER NOT NULL,
	start_date date NOT NULL,
	"type" varchar NOT NULL,
	course_ref BIGINT NOT NULL,
	teacher_ref BIGINT NULL,
	account_ref BIGINT NOT NULL,
  tag varchar NOT NULL,
	CONSTRAINT pk_class_plan PRIMARY KEY (class_plan_id),
	CONSTRAINT fk_class_plan_account FOREIGN KEY (account_ref) REFERENCES public.account(account_id),
	CONSTRAINT fk_class_plan_course FOREIGN KEY (course_ref) REFERENCES public.course(course_id),
	CONSTRAINT fk_class_plan_teacher FOREIGN KEY (teacher_ref) REFERENCES public.teacher(teacher_id)
)
WITH (
	OIDS=FALSE
);

CREATE TABLE public.course (
	course_id BIGINT NOT NULL DEFAULT nextval('course_course_id_seq'::regclass),
	academic_year INTEGER NOT NULL,
	semester INTEGER NOT NULL,
	subject_ref BIGINT NOT NULL,
	CONSTRAINT pk_course PRIMARY KEY (course_id),
	CONSTRAINT fk_course_subject FOREIGN KEY (subject_ref) REFERENCES public.subject(subject_id)
)
WITH (
	OIDS=FALSE
);

CREATE TABLE public.databasechangelog (
	id varchar(255) NOT NULL,
	author varchar(255) NOT NULL,
	filename varchar(255) NOT NULL,
	dateexecuted timestamptz NOT NULL,
	orderexecuted INTEGER NOT NULL,
	exectype varchar(10) NOT NULL,
	md5sum varchar(35) NULL,
	description varchar(255) NULL,
	comments varchar(255) NULL,
	tag varchar(255) NULL,
	liquibase varchar(20) NULL
)
WITH (
	OIDS=FALSE
);

CREATE TABLE public.databasechangeloglock (
	id INTEGER NOT NULL,
	locked bool NOT NULL,
	lockgranted timestamptz NULL,
	lockedby varchar(255) NULL,
	CONSTRAINT pk_databasechangeloglock PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
);

CREATE TABLE public.department (
	department_id BIGINT NOT NULL DEFAULT nextval('department_department_id_seq'::regclass),
	"name" varchar NOT NULL,
	CONSTRAINT pk_department PRIMARY KEY (department_id)
)
WITH (
	OIDS=FALSE
);

CREATE TABLE public.generic_subject (
	generic_subject_id BIGINT NOT NULL DEFAULT nextval('generic_subject_generic_subject_id_seq'::regclass),
	"name" varchar NOT NULL,
	CONSTRAINT pk_generic_subject PRIMARY KEY (generic_subject_id)
)
WITH (
	OIDS=FALSE
);

CREATE TABLE public.person (
	person_id BIGINT NOT NULL DEFAULT nextval('person_person_id_seq'::regclass),
	first_name varchar NOT NULL,
	second_name varchar NOT NULL,
	patronymic varchar NULL,
	image_url varchar NULL,
	account_ref BIGINT NULL,
	"type" varchar NOT NULL,
	CONSTRAINT pk_person PRIMARY KEY (person_id),
	CONSTRAINT fk_person_account FOREIGN KEY (account_ref) REFERENCES public.account(account_id)
)
WITH (
	OIDS=FALSE
);
CREATE INDEX ix_person_first_name_second_name_patronymic ON public.person (first_name,second_name,patronymic);
CREATE INDEX ix_person_second_name_first_name_patronymic ON public.person (second_name,first_name,patronymic);

CREATE TABLE public.plan_element (
	plan_element_id BIGINT NOT NULL DEFAULT nextval('plan_element_plan_element_id_seq'::regclass),
	weekday varchar NOT NULL,
	class_plan_ref BIGINT NOT NULL,
	room_ref BIGINT NULL,
	"number" INTEGER NOT NULL,
	CONSTRAINT pk_plan_element PRIMARY KEY (plan_element_id),
	CONSTRAINT fk_plan_element_class_plan FOREIGN KEY (class_plan_ref) REFERENCES public.class_plan(class_plan_id),
	CONSTRAINT fk_plan_element_room FOREIGN KEY (room_ref) REFERENCES public.room(room_id)
)
WITH (
	OIDS=FALSE
);

CREATE TABLE public.room (
	room_id BIGINT NOT NULL DEFAULT nextval('room_room_id_seq'::regclass),
	"number" varchar NOT NULL,
	building_ref BIGINT NOT NULL,
	CONSTRAINT pk_room PRIMARY KEY (room_id),
	CONSTRAINT fk_room_buliding FOREIGN KEY (building_ref) REFERENCES public.building(building_id)
)
WITH (
	OIDS=FALSE
);

CREATE TABLE public.student (
	student_id BIGINT NOT NULL DEFAULT nextval('student_student_id_seq'::regclass),
	academic_group_ref BIGINT NOT NULL,
	CONSTRAINT pk_student PRIMARY KEY (student_id),
	CONSTRAINT fk_student_academic_group FOREIGN KEY (academic_group_ref) REFERENCES public.academic_group(academic_group_id),
	CONSTRAINT fk_student_person FOREIGN KEY (student_id) REFERENCES public.person(person_id)
)
WITH (
	OIDS=FALSE
);

CREATE TABLE public.student_visitor (
	student_visitor_id BIGINT NOT NULL DEFAULT nextval('student_visitor_student_visitor_id_seq'::regclass),
	visitors_group_ref BIGINT NOT NULL,
	student_ref BIGINT NOT NULL,
	CONSTRAINT pk_student_visitor PRIMARY KEY (student_visitor_id),
	CONSTRAINT student_visitor_student_ref_visitors_group_ref_key UNIQUE (student_ref,visitors_group_ref),
	CONSTRAINT fk_student_visitor_class_plan FOREIGN KEY (visitors_group_ref) REFERENCES public.class_plan(class_plan_id),
	CONSTRAINT fk_student_visitor_student FOREIGN KEY (student_ref) REFERENCES public.student(student_id)
)
WITH (
	OIDS=FALSE
);

CREATE TABLE public.subject (
	subject_id BIGINT NOT NULL DEFAULT nextval('subject_subject_id_seq'::regclass),
	"name" varchar NOT NULL,
	generic_subject_ref BIGINT NOT NULL,
	CONSTRAINT pk_subject PRIMARY KEY (subject_id),
	CONSTRAINT fk_teacher_generic_subject FOREIGN KEY (generic_subject_ref) REFERENCES public.generic_subject(generic_subject_id)
)
WITH (
	OIDS=FALSE
);

CREATE TABLE public.teacher (
	teacher_id BIGINT NOT NULL DEFAULT nextval('teacher_teacher_id_seq'::regclass),
	CONSTRAINT pk_teacher PRIMARY KEY (teacher_id),
	CONSTRAINT fk_teacher_person FOREIGN KEY (teacher_id) REFERENCES public.person(person_id)
)
WITH (
	OIDS=FALSE
);

CREATE TABLE public.teacher_generic_subject (
	teacher_generic_subject_id BIGINT NOT NULL DEFAULT nextval('teacher_generic_subject_teacher_generic_subject_id_seq'::regclass),
	generic_subject_ref BIGINT NOT NULL,
	teacher_ref BIGINT NOT NULL,
	CONSTRAINT pk_teacher_generic_subject PRIMARY KEY (teacher_generic_subject_id),
	CONSTRAINT fk_teacher_generic_subject_generic_subject FOREIGN KEY (generic_subject_ref) REFERENCES public.generic_subject(generic_subject_id),
	CONSTRAINT fk_teacher_generic_subject_teacher FOREIGN KEY (teacher_ref) REFERENCES public.teacher(teacher_id)
)
WITH (
	OIDS=FALSE
);

-- DROP SEQUENCE public.academic_flow_academic_flow_id_seq;

CREATE SEQUENCE public.academic_flow_academic_flow_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

-- DROP SEQUENCE public.academic_group_academic_group_id_seq;

CREATE SEQUENCE public.academic_group_academic_group_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

-- DROP SEQUENCE public.account_account_id_seq;

CREATE SEQUENCE public.account_account_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

-- DROP SEQUENCE public.account_department_account_department_id_seq;

CREATE SEQUENCE public.account_department_account_department_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

-- DROP SEQUENCE public.app_property_app_property_id_seq;

CREATE SEQUENCE public.app_property_app_property_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

-- DROP SEQUENCE public.assignment_assignment_id_seq;

CREATE SEQUENCE public.assignment_assignment_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

-- DROP SEQUENCE public.assignment_grade_assignment_grade_id_seq;

CREATE SEQUENCE public.assignment_grade_assignment_grade_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

-- DROP SEQUENCE public.building_building_id_seq;

CREATE SEQUENCE public.building_building_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

-- DROP SEQUENCE public.class_class_id_seq;

CREATE SEQUENCE public.class_class_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

-- DROP SEQUENCE public.class_plan_class_plan_id_seq;

CREATE SEQUENCE public.class_plan_class_plan_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

-- DROP SEQUENCE public.course_course_id_seq;

CREATE SEQUENCE public.course_course_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

-- DROP SEQUENCE public.department_department_id_seq;

CREATE SEQUENCE public.department_department_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

-- DROP SEQUENCE public.generic_subject_generic_subject_id_seq;

CREATE SEQUENCE public.generic_subject_generic_subject_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

-- DROP SEQUENCE public.person_person_id_seq;

CREATE SEQUENCE public.person_person_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

-- DROP SEQUENCE public.plan_element_plan_element_id_seq;

CREATE SEQUENCE public.plan_element_plan_element_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

-- DROP SEQUENCE public.room_room_id_seq;

CREATE SEQUENCE public.room_room_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

-- DROP SEQUENCE public.student_student_id_seq;

CREATE SEQUENCE public.student_student_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

-- DROP SEQUENCE public.student_visitor_student_visitor_id_seq;

CREATE SEQUENCE public.student_visitor_student_visitor_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

-- DROP SEQUENCE public.subject_subject_id_seq;

CREATE SEQUENCE public.subject_subject_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

-- DROP SEQUENCE public.teacher_generic_subject_teacher_generic_subject_id_seq;

CREATE SEQUENCE public.teacher_generic_subject_teacher_generic_subject_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

-- DROP SEQUENCE public.teacher_teacher_id_seq;

CREATE SEQUENCE public.teacher_teacher_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

