<% request.setAttribute("title", "Teachers");%>
<%@ include file="templ/header.jsp" %>

<div class="container panel list">
    <div class="panel">
        <div style="display:inline-block;">
            <h2>${item.subject_name}</h2>
            <h3>Average grade by this subject: ${item.avg_grade}</h3>
            <h3>Description:</h3>
            <h4>${item.description}
        </div>
        <h3>Students, who enrolled this subject, also choosed:</h3>
        <c:forEach var="subject" items="${subjects}">
            <div class="panel">
                <a href="/subject/${subject.subject_id}"><h4>${subject.subject_name}</h4></a>
            </div>
        </c:forEach>
    </div>
</div>

<%@ include file="templ/footer.jsp" %>