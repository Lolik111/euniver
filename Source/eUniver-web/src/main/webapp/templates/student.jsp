<% request.setAttribute("title", "Student info");%>
<%@ include file="templ/header.jsp" %>

	<div class="container panel list">
		<div class="panel">
			<div style="display:inline-block;">
				<img src="${item.image_url}" width="200" height="200"/>
			</div>
			<div style="display:inline-block;">
				<h2>${item.first_name} ${item.second_name}</h2>
				<p>
					Group: <a href="/group/${item.academic_group_id}">${item.name}</a>
				</p>
			</div>
			<h3>Courses:</h3>
			<c:forEach var="course" items="${courses}">
				<div class="panel">
						<a href="/course/${course.course_id}"><h4>${course.subject_name} ${course.academic_year}</h4></a>
						Average Grade : ${course.avg_grade}
				</div>
			</c:forEach>
		</div>
	</div>

<%@ include file="templ/footer.jsp" %>