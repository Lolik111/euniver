<% request.setAttribute("title", "Students");%>
<%@ include file="templ/header.jsp" %>

<div class="container panel list">
    <c:forEach var="item" items="${list}">
        <div class="panel">
            <div style="display:inline-block;">
                <img src="${item.image_url}" width="200" height="200"/>
            </div>
            <div style="display:inline-block;">
                <a href="/student/${item.student_id}"><h3>${item.first_name} ${item.second_name}</h3></a>
                <p>
                    Group: <a href="/group/${item.academic_group_id}">${item.name}</a>
                </p>
            </div>
        </div>
    </c:forEach>
</div>

<%@ include file="templ/footer.jsp" %>