<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="include.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>${title}</title>
    <link href="<c:url value='/bootstrap/bootstrap-3.3.6/css/bootstrap.css' />"  rel="stylesheet"/>
    <link href="<c:url value='/css/main.css' />" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/bootstrap/bootstrap-3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">

        <!-- Static navbar -->
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <img alt="Innopolis University" src="/images/logo.png" width="75" >
                    </a>
                    <a class="navbar-brand" href="/">eUniver</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="/home">Home</a></li>
                        <sec:authorize access="hasAnyRole('TEACHER')">
                            <li><a href="/cabinet">Cabinet</a></li>
                        </sec:authorize>
                        <sec:authorize access="hasAnyRole('ADMIN', 'DEPARTMENT_ADMIN')">
                            <li><a href="/admin">Admin</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Add <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/newStudent">Add Student</a></li>
                                    <li><a href="/newTeacher">Add Teacher</a></li>
                                </ul>
                            </li>
                            <li><a href="/teacher">Teachers</a></li>
                            <li><a href="/student">Students</a></li>
                            <li><a href="/group">Groups</a></li>
                            <li><a href="/course">Courses</a></li>
                            <li><a href="/subject">Subjects</a></li>
                        </sec:authorize>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <sec:authorize access="hasAnyRole('ADMIN', 'DEPARTMENT_ADMIN', 'TEACHER')">
                            <li><span>Signed in as <a href="#" class="navbar-link"><b><sec:authentication property="principal.username"/></b></a></span></li>
                            <li>
                                <a href="/logout">
                                    <button type="button" class="btn btn-default navbar-btn">Log out</button>
                                </a>
                            </li>
                        </sec:authorize>
                    </ul>
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>
