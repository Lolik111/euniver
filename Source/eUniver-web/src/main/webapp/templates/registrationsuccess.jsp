<% request.setAttribute("title", "User Registration Form");%>
<%@ include file="templ/header.jsp" %>

	<div class="success">
		Confirmation message : ${success}
		<br>
		Would you like to add more <a href="<c:url value='/newStudent'/>">Add Student</a> or <a href="<c:url value='/newTeacher'/>">Add Teacher</a> ?
		<br/>
		Go to <a href="<c:url value='/admin' />">Admin Page</a> OR <a href="<c:url value="/logout" />">Logout</a>	
	</div>

<%@ include file="templ/footer.jsp" %>