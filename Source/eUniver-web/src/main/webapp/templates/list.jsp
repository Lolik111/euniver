<% request.setAttribute("title", "Simple list");%>
<%@ include file="templ/header.jsp" %>

	<div class="container panel list">
		<c:forEach var="item" items="${list}">
			<div class="panel">
				<c:forEach items="${item}" var="entry">
					<h3>
						<span style="text-transform: capitalize">
							${entry.key.replace("_", " ")}:
						</span> <b>${entry.value}</b>
					</h3>
					<br>
				</c:forEach>
			</div>
		</c:forEach>
	</div>

<%@ include file="templ/footer.jsp" %>