<% request.setAttribute("title", "Admin page");%>
<%@ include file="templ/header.jsp" %>

	<div class="success">
		Dear <strong>${user}</strong>, Welcome to Admin Page.
		<br/>
		Would you like to <a href="<c:url value='/newStudent'/>">Add Student</a> or <a href="<c:url value='/newTeacher'/>">Add Teacher</a> to keep yourself busy?
		<br/>
		<a href="<c:url value="/logout" />">Logout</a>
	</div>

<%@ include file="templ/footer.jsp" %>