<% request.setAttribute("title", "Subject");%>
<%@ include file="templ/header.jsp" %>

<div class="container panel list">
    <div class="panel">
        <c:forEach var="item" items="${list}">
            <div class="panel">
                <div style="display:inline-block;">
                    <a href="/subject/${item.id}"><h2>${item.name}</h2></a>
                </div>
            </div>
        </c:forEach>
    </div>
</div>

<%@ include file="templ/footer.jsp" %>