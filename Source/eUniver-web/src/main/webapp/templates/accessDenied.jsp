<% request.setAttribute("title", "Access Denied");%>
<%@ include file="templ/header.jsp" %>

	<br/>
	<br/>
	<h1>Dear <strong>${user}</strong>, You are not authorized to access this page.</h1>
	<br/>
	<a href="<c:url value="/home" />">Go to home</a>  
	<sec:authorize access="hasRole('USER') or hasRole('ADMIN') or hasRole('DBA')">
           OR  <a href="<c:url value="/logout" />">Logout</a>
    </sec:authorize>

<%@ include file="templ/footer.jsp" %>