<% request.setAttribute("title", "Assignment");%>
<%@ include file="templ/header.jsp" %>

<div class="container panel list">
    <div class="panel">
        <div style="display:inline-block;">
            <h4>${plan.subject_name} ${plan.academic_year} ${plan.type}</h4>
            taught for
            <a href="#/attendants/${plan.class_plan_id}"><h4>${plan.tag}</h4></a>
        </div>
        <br/>
        <div style="display:inline-block;">
            <h4>Assignment #${assignment.assignment_id} from ${assignment.creation_date}</h4>
            <h4>Description: ${assignment.description}</h4>
            <a href="${assignment.materials_url}"><h4>Materials</h4></a>
        </div>


        <form:form method="POST" commandName="grades" class="form-horizontal">

            <c:forEach items="${grades.objects}" var="grade">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div class="col-md-7 ">
                            <div style="display:inline-block;">
                                <img src="${grade.map.image_url}" width="200" height="200"/>
                            </div>
                            <div style="display:inline-block;">
                                <a href="/student/${grade.map.student_id}">
                                    <h3>${grade.map.first_name} ${grade.map.second_name}</h3>
                                </a>
                                <p>
                                    Group: <a href="/group/${grade.map.academic_group_id}">${grade.map.group_name}</a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <h3>Grade:</h3>
                            <form:input type="number" path="objects[${grade.map.row_id}].map['grade']" id="grade${grade.map.row_id}"
                                        class="form-control input-sm"/>
                            <form:input type="hidden" path="objects[${grade.map.row_id}].map['student_id']" value="${grade.map.student_id}"
                                        class="form-control input-sm"/>
                            <%--<div class="has-error">--%>
                                <%--<form:errors path="grade${grade.map.row_id}" class="help-inline"/>--%>
                            <%--</div>--%>
                        </div>
                    </div>
                </div>
            </c:forEach>


            <div class="row">
                <div class="form-actions floatRight">
                    <input type="submit" value="Update" class="btn btn-primary btn-sm">
                </div>
            </div>
        </form:form>

        <%--<form:form method="POST" commandName="mmap" class="form-horizontal">--%>
        <%--<form:input type="text" path="map['description']"  />--%>


        <%--<div class="row">--%>
        <%--<div class="form-actions floatRight">--%>
        <%--<input type="submit" value="Update" class="btn btn-primary btn-sm">--%>
        <%--</div>--%>
        <%--</div>--%>
        <%--</form:form>--%>


    </div>

</div>

<%@ include file="templ/footer.jsp" %>