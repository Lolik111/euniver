<% request.setAttribute("title", "Course rating");%>
<%@ include file="templ/header.jsp" %>

	<div class="container panel list">
		<div class="panel">
			<div style="display:inline-block;">
				<h2>${item.subject_name} ${item.academic_year} / ${item.semester}</h2>
			</div>
			<h3>Student rating: </h3>
			<c:forEach var="elem" items="${list}">
				<div class="panel">
					<div  style="display:inline-block;">
						<img src="${elem.image_url}" width="200" height="200"/>
					</div>
					<div style="display:inline-block;">
						<a href="/student/${elem.student_id}"><h3>${elem.first_name} ${elem.second_name}</h3></a>
					</div>
					<p>
						Group: <a href="/group/${elem.academic_group_id}">${elem.group_name}</a>
					</p>
					<p>
						Average grade: ${elem.grade}
					</p>
				</div>
			</c:forEach>
		</div>
	</div>

<%@ include file="templ/footer.jsp" %>