<% request.setAttribute("title", "Teachers");%>
<%@ include file="templ/header.jsp" %>

<div class="container panel list">
    <div class="panel">
        <c:forEach var="item" items="${list}">
        <div class="panel">
            <div style="display:inline-block;">
                <img src="${item.imageUrl}" width="200" height="200"/>
            </div>
            <div style="display:inline-block;">
                <a href="#/teacher/${item.id}"><h2>${item.firstName} ${item.secondName}</h2></a>
            </div>
        </div>
    </c:forEach>
    </div>
</div>

<%@ include file="templ/footer.jsp" %>