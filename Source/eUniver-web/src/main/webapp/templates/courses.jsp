<% request.setAttribute("title", "Courses");%>
<%@ include file="templ/header.jsp" %>.


    <div class="container panel list">
        <c:forEach var="item" items="${list}">
            <div class="panel">
                <div style="display:inline-block;">
                    <a href="/course/${item.course_id}"><h3>${item.subject_name} ${item.academic_year} / ${item.semester}</h3></a>
                    <p>
                        Subject: <a href="/subject/${item.subject_id}">${item.subject_name}</a>
                    </p>
                    <p>
                        Teachers: ${item.teacher_count}
                    </p>
                    <p>
                        Students: ${item.student_count}
                    </p>
                </div>
            </div>
        </c:forEach>
    </div>

<%@ include file="templ/footer.jsp" %>