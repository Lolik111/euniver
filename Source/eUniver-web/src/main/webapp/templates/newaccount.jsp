<% request.setAttribute("title", "User Registration Form");%>
<%@ include file="templ/header.jsp" %>

<div class="success" >
    Confirmation message : ${success}
    Go to <a href="<c:url value='/admin' />">Admin Page</a> OR <a href="<c:url value="/logout" />">Logout</a>
</div>

<div class="form-container">

    <h1>New Account Registration Form</h1>

    <form:form method="POST" modelAttribute="account" class="form-horizontal">

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="login">Login</label>
                <div class="col-md-7">
                    <form:input type="text" path="login" id="login" class="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="login" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="password">Password</label>
                <div class="col-md-7">
                    <form:input type="password" path="password" id="password" class="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="password" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="type">Roles</label>
                <div class="col-md-7">
                    <form:select path="type" items="${roles}" multiple="false" itemLabel="type"
                                 class="form-control input-sm" />
                    <div class="has-error">
                        <form:errors path="type" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-actions floatRight">
                <input type="submit" value="Register" class="btn btn-primary btn-sm"> or <a href="<c:url value='/admin' />">Cancel</a>
            </div>
        </div>
    </form:form>
</div>

<%@ include file="templ/footer.jsp" %>