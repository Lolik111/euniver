<% request.setAttribute("title", "Group rating");%>
<%@ include file="templ/header.jsp" %>

	<div class="container panel list">
		<div class="panel">
			<div style="display:inline-block;">
				<h2>${item.academic_group_name}</h2>
				<p>
					Department: <a href="#/department/${item.department_id}">${item.department_name}</a>
				</p>
			</div>
			<h3>Student rating: </h3>
			<c:forEach var="elem" items="${list}">
				<div class="panel">
					<div  style="display:inline-block;">
						<img src="${elem.image_url}" width="200" height="200"/>
					</div>
					<div style="display:inline-block;">
						<a href="/student/${elem.student_id}"><h3>${elem.first_name} ${elem.second_name}</h3></a>
					</div>
					<p>
						Average grade: ${elem.grade}
					</p>
				</div>
			</c:forEach>
		</div>
	</div>

<%@ include file="templ/footer.jsp" %>