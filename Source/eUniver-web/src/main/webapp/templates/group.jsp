<% request.setAttribute("title", "Group");%>
<%@ include file="templ/header.jsp" %>

	<div class="container panel list">
		<div class="panel">
			<div style="display:inline-block;">
				<h2>${item.academic_group_name}</h2>
				<p>
					Department: <a href="#/department/${item.department_id}">${item.department_name}</a>
				</p>
			</div>
            <div>
                <a href="/group/${requestScope.id}/rating"><button class="btn btn-info">Rating</button></a>
            </div>
			<h3>Students: ${item.student_count}</h3>
			<c:forEach var="elem" items="${list}">
				<div class="panel">
					<div  style="display:inline-block;">
						<img src="${elem.image_url}" width="200" height="200"/>
					</div>
					<div style="display:inline-block;">
						<a href="/student/${elem.student_id}"><h3>${elem.first_name} ${elem.second_name}</h3></a>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>

<%@ include file="templ/footer.jsp" %>