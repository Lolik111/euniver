<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="templ/include.jsp" %>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Login page</title>
  <link href="<c:url value='/bootstrap/bootstrap-3.3.6/css/bootstrap.css' />"  rel="stylesheet"/>
  <link href="<c:url value='/css/main.css' />" rel="stylesheet"/>
  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css" />
</head>

<body>
<div id="mainWrapper">
  <div class="login-container">
    <div class="login-card">
      <div class="login-form">
        <c:url var="loginUrl" value="/login" />
        <form:form action="${loginUrl}" method="POST" class="form-horizontal">
          <c:if test="${param.error != null}">
            <div class="alert alert-danger">
              <p>Invalid username and password.</p>
            </div>
          </c:if>
          <c:if test="${param.mercy != null}">
            <div class="alert alert-danger">
              <p>You attempted to log in too much in last 10 minutes</p>
            </div>
          </c:if>
          <c:if test="${param.logout != null}">
            <div class="alert alert-success">
              <p>You have been logged out successfully.</p>
            </div>
          </c:if>
          <div class="input-group input-sm">
            <label class="input-group-addon" for="username"><i class="fa fa-user"></i></label>
            <input type="text" class="form-control" id="username" name="username" placeholder="Enter Username" required>
          </div>
          <div class="input-group input-sm">
            <label class="input-group-addon" for="password"><i class="fa fa-lock"></i></label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" required>
          </div>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

          <div class="form-actions">
            <input type="submit" class="btn btn-block btn-primary btn-default" value="Log in">
          </div>
        </form:form>
      </div>
    </div>
  </div>
</div>

</body>
</html>