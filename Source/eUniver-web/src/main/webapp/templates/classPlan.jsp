<% request.setAttribute("title", "Plan");%>
<%@ include file="templ/header.jsp" %>

<div class="container panel list">
    <div class="panel">
        <div style="display:inline-block;">
            <a href="/subject/${plan.subject_id}"><h4>${plan.subject_name}</h4></a> <h4> ${plan.academic_year} ${plan.type}</h4>
            taught for
            <a href="#/attendants/${plan.class_plan_id}"><h4>${plan.tag}</h4></a>
        </div>
        <div class="row">
            <div class="col-md-6 ">
                <h2>Classes:</h2>
                <c:forEach var="sclass" items="${classes}">
                    <div class="panel">
                        <h4>${sclass.class_number} ${sclass.date} ${sclass.time}</h4>
                        in <a href="#/room/${sclass.room_id}"><h4>${sclass.room_number} ${sclass.building_number} </h4></a>
                    </div>
                </c:forEach>
            </div>

            <div class="col-md-6">
                <h2>Assignments:</h2>
                <c:forEach var="assignment" items="${assignments}">
                    <div class="panel">
                        <a href="/assignment/${assignment.assignment_id}">
                            <h4>Assignment #${assignment.assignment_id} from ${assignment.creation_date}</h4></a>
                    </div>
                </c:forEach>
            </div>

        </div>
    </div>
</div>

<%@ include file="templ/footer.jsp" %>