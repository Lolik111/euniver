<% request.setAttribute("title", "Groups");%>
<%@ include file="templ/header.jsp" %>


    <div class="container panel list">
        <c:forEach var="item" items="${list}">
            <div class="panel">
                <div style="display:inline-block;">
                    <a href="/group/${item.academic_group_id}"><h3>${item.academic_group_name}</h3></a>
                    <p>
                        Department: <a href="#/department/${item.department_id}">${item.department_name}</a>
                    </p>
                    <p>
                        Students: ${item.student_count}
                    </p>
                </div>
            </div>
        </c:forEach>
    </div>

<%@ include file="templ/footer.jsp" %>