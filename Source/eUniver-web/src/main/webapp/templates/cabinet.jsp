<% request.setAttribute("title", "Cabinet");%>
<%@ include file="templ/header.jsp" %>

<div class="container panel list">
    <div class="panel">
        <div  style="display:inline-block;">
            <img src="${item.imageUrl}" width="200" height="200"/>
        </div>
        <div style="display:inline-block;">
            <h2>${item.firstName} ${item.secondName}</h2>
        </div>
        <h3>Taught plans:</h3>
        <c:forEach var="course" items="${classPlans}">
            <div class="panel">
                <a href="/cabinet/plan/${course.class_plan_id}"><h4>${course.subject_name} ${course.academic_year} ${course.type}</h4></a>
                taught for
                <a href="#/attendants/${course.class_plan_id}"><h4>${course.tag}</h4></a>
                <h4>Average smoothed grade: ${course.grade}</h4>
            </div>
        </c:forEach>
    </div>
</div>

<%@ include file="templ/footer.jsp" %>