<% request.setAttribute("title", "User Registration Form");%>
<%@ include file="templ/header.jsp" %>

<div class="form-container">

    <h1>New Teacher Registration Form</h1>

    <form:form method="POST" modelAttribute="teacher" class="form-horizontal">

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="firstName">First Name</label>
                <div class="col-md-7">
                    <form:input type="text" path="firstName" id="firstName" class="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="firstName" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="secondName">Last Name</label>
                <div class="col-md-7">
                    <form:input type="text" path="secondName" id="secondName" class="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="secondName" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="patronymic">Patronymic</label>
                <div class="col-md-7">
                    <form:input type="text" path="patronymic" id="patronymic" class="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="patronymic" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="imageUrl">Image Url</label>
                <div class="col-md-7">
                    <form:input type="text" path="imageUrl" id="imageUrl" class="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="imageUrl" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>


        <%--<div class="row">--%>
        <%--<div class="form-group col-md-12">--%>
        <%--<label class="col-md-3 control-lable" for="email">Email</label>--%>
        <%--<div class="col-md-7">--%>
        <%--<form:input type="text" path="email" id="email" class="form-control input-sm"/>--%>
        <%--<div class="has-error">--%>
        <%--<form:errors path="email" class="help-inline"/>--%>
        <%--</div>--%>
        <%--</div>--%>
        <%--</div>--%>
        <%--</div>--%>


        <%--<div class="row">--%>
        <%--<div class="form-group col-md-12">--%>
        <%--<label class="col-md-3 control-lable" for="type">Roles</label>--%>
        <%--<div class="col-md-7">--%>
        <%--<form:select path="type" items="${roles}" multiple="true" itemLabel="type" class="form-control input-sm"/>--%>
        <%--<div class="has-error">--%>
        <%--<form:errors path="type" class="help-inline"/>--%>
        <%--</div>--%>
        <%--</div>--%>
        <%--</div>--%>
        <%--</div>--%>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="subjects">General subjects</label>
                <div class="col-md-7">
                    <form:select path="genericSubjects" items="${subjects}" id="subjects" multiple="true" itemLabel="name" itemValue="id" class="form-control input-sm"/>
                        <%--<div class="has-error">--%>
                        <%--<form:errors path="type" class="help-inline"/>--%>
                        <%--</div>--%>
                </div>
            </div>
        </div>

        <%--<c:forEach items="${groups}" var="group">--%>
        <%--<tr>--%>
        <%--<td><c:out value="${group.name}" /></td>--%>
        <%--</tr>--%>
        <%--</c:forEach>--%>

        <div class="row">
            <div class="form-actions floatRight">
                <input type="submit" value="Register" class="btn btn-primary btn-sm"> or <a href="<c:url value='/admin' />">Cancel</a>
            </div>
        </div>
    </form:form>
</div>

<%@ include file="templ/footer.jsp" %>