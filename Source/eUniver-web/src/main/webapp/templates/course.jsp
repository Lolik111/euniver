<% request.setAttribute("title", "Course");%>
<%@ include file="templ/header.jsp" %>

<div class="container panel list">
    <div style="display:inline-block;">
        <h2>${item.subject_name} ${item.academic_year} / ${item.semester}</h2>
    </div>
    <div>
        <p>
            Subject: <a href="/subject/${item.subject_id}">${item.subject_name}</a>
        </p>
        <a href="/course/${requestScope.id}/rating">
            <button class="btn btn-info">Rating</button>
        </a>
    </div>

    <h3>Teachers: ${item.student_count}</h3>
    <c:forEach var="elem" items="${teachers}">
        <div class="panel">
            <div style="display:inline-block;">
                <img src="${elem.image_url}" width="200" height="200"/>
            </div>
            <div style="display:inline-block;">
                <a href="#/teacher/${elem.teacher_id}"><h3>${elem.first_name} ${elem.second_name}</h3></a>
            </div>
        </div>
        <p>
            Class types: ${elem.types}
        </p>
    </c:forEach>
    <div style="height: 75px"></div>
</div>

<div class="container panel list">
    <h3>Students: ${item.student_count}</h3>
    <c:forEach var="elem" items="${list}">
        <div class="panel">
            <div style="display:inline-block;">
                <img src="${elem.image_url}" width="200" height="200"/>
            </div>
            <div style="display:inline-block;">
                <a href="/student/${elem.student_id}"><h3>${elem.first_name} ${elem.second_name}</h3></a>
            </div>
            <p>
                Group: <a href="/group/${elem.academic_group_id}">${elem.group_name}</a>
            </p>
        </div>
    </c:forEach>
</div>

<%@ include file="templ/footer.jsp" %>