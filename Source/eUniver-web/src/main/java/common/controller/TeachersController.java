package common.controller;

import common.controller.services.interfaces.AccountService;
import common.controller.services.interfaces.SimpleService;
import common.controller.services.interfaces.StudentService;
import common.controller.services.interfaces.TeacherService;
import common.model.JustAListOfJustMaps;
import common.model.JustAMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import university.domain.Account;
import university.domain.Person;
import university.domain.Teacher;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Damir on 04.07.16.
 */
@Controller
public class TeachersController {

    private static final int stupidConstant = 70;

    @Autowired
    private SimpleService simpleService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private StudentService studentService;



    @RequestMapping(value = "/teacher", method = RequestMethod.GET)
    public String getTeachers(Model model) {
        model.addAttribute("list", simpleService.fetchAll(Teacher.class));
        return "teachers";
    }

    @RequestMapping(value = "/cabinet", method = RequestMethod.GET)
    public String getCabinet(Model model) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Account account = accountService.findAccWithPersonByLogin(username);
        if(account.getPerson().getType() == Person.Type.STUDENT){
            return "accessDenied";
        }

        List<Map<String, Object>> classPlans = teacherService.fetchTeachersClassPlans(account.getPerson().getId());
        List<Map<String, Object>> grades = teacherService.fetchSmoothedGradesForPlanByTeacher(account.getPerson().getId(), stupidConstant);
        for (Map<String, Object> map : classPlans){
            for(Map<String, Object> grade : grades){
                if(grade.get("class_plan_id").equals(map.get("class_plan_id"))){
                    map.put("grade", grade.get("avg_grade"));
                    break;
                }
            }
        }

        model.addAttribute("item", account.getPerson());
        model.addAttribute("classPlans", classPlans);

        return "cabinet";
    }

    @RequestMapping(value = "/cabinet/plan/{id}", method = RequestMethod.GET)
    public String getPlan(Model model, @PathVariable("id") Long id) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Account account = accountService.findAccWithPersonByLogin(username);
        if(account.getPerson().getType() == Person.Type.STUDENT){
            return "accessDenied";
        }

        final List<Long> ids = teacherService.fetchTeachersClassPlanIds(account.getPerson().getId());
        if(!ids.contains(id)){
            return "accessDenied";
        }

        Map<String, Object> classPlan = teacherService.fetchSingleClassPlan(id);
        List<Map<String, Object>> classes = teacherService.fetchClassesInfo(id);
        List<Map<String, Object>> assignments = teacherService.fetchAssignmentsInfo(id);


        model.addAttribute("plan", classPlan);
        model.addAttribute("classes", classes);
        model.addAttribute("assignments", assignments);

        return "classPlan";
    }

    @RequestMapping(value = "/assignment/{id}", method = RequestMethod.GET)
    public String getAssignmentR(Model model, @PathVariable("id") Long id) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Account account = accountService.findAccWithPersonByLogin(username);
        if(account.getPerson().getType() == Person.Type.STUDENT){
            return "accessDenied";
        }

        final Long classPlanId = teacherService.getClassPlanByAssignmentId(id);

        final List<Long> ids = teacherService.fetchTeachersClassPlanIds(account.getPerson().getId());
        if(!ids.contains(classPlanId)){
            return "accessDenied";
        }

        Map<String, Object> classPlan = teacherService.fetchSingleClassPlan(classPlanId);
        Map<String, Object> assignment = teacherService.fetchAssignment(id);
        List<Map<String, Object>> ingrades = teacherService.fetchGradesByAssignment(id);
        JustAListOfJustMaps grades = new JustAListOfJustMaps();
        List<JustAMap> lst = new ArrayList<>();
        for (Map<String, Object> v : ingrades) {
            JustAMap j = new JustAMap();
            j.setMap(v);
            lst.add(j);
        }
        grades.setObjects(lst);


        model.addAttribute("plan", classPlan);
        model.addAttribute("assignment", assignment);
        model.addAttribute("grades", grades);

        return "assignment";
    }

    @RequestMapping(value = "/assignment/{id}", method = RequestMethod.POST)
    public String postAssignmentR(@ModelAttribute("justAListOfObjects") JustAListOfJustMaps justAListOfJustMaps, BindingResult result, Model model, @PathVariable("id") Long id) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Account account = accountService.findAccWithPersonByLogin(username);
        if(account.getPerson().getType() == Person.Type.STUDENT){
            return "accessDenied";
        }

        final Long classPlanId = teacherService.getClassPlanByAssignmentId(id);

        final List<Long> ids = teacherService.fetchTeachersClassPlanIds(account.getPerson().getId());
        if(!ids.contains(classPlanId)){
            return "accessDenied";
        }

        for (JustAMap map:justAListOfJustMaps.getObjects()) {
            String stringId = (String) map.getMap().get("student_id");
            String stringGrade = (String) map.getMap().get("grade");
            Integer grade = null;
            try{
                grade = Integer.parseInt(stringGrade);
            } catch (NumberFormatException e) {
            }
            Long studentId = Long.parseLong(stringId);

            teacherService.updateGradeByAssignment(studentId, id, grade);
        }
        return getAssignmentR(model, id);
    }


    public String getAttendants(Model model, @PathVariable("id") String id) {
        model.addAttribute("list", studentService.fetchStudentsGroups());
        return "students";
    }





}
