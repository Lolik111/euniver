package common.controller;

import common.controller.services.interfaces.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Albert Valiullin on 19.11.2016.
 */
@Controller
public class GroupController {
    @Autowired
    private GroupService groupService;

    @RequestMapping(value = "/group", method = RequestMethod.GET)
    public String getGroups(Model model) {
        model.addAttribute("list", groupService.fetchGroups());
        return "groups";
    }

    @RequestMapping(value = "/group/{id}", method = RequestMethod.GET)
    public String getGroup(Model model, @PathVariable("id") Long id) {
        model.addAttribute("item", groupService.fetchById(id));
        model.addAttribute("list", groupService.groupStudents(id));
        return "group";
    }

    @RequestMapping(value = "/group/{id}/rating", method = RequestMethod.GET)
    public String getRating(Model model, @PathVariable("id") Long id) {
        model.addAttribute("item", groupService.fetchById(id));
        model.addAttribute("list", groupService.groupStudentsRating(id));
        return "grouprating";
    }


}
