package common.controller;

import common.controller.services.interfaces.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Albert Valiullin on 18.11.2016.
 */
@Controller
public class StudentsController {

    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "/student", method = RequestMethod.GET)
    public String getStudents(Model model) {
        model.addAttribute("list", studentService.fetchStudentsGroups());
        return "students";
    }

    @RequestMapping(value = "/student/{id}", method = RequestMethod.GET)
    public String getStudent(Model model, @PathVariable("id") Long id) {
        model.addAttribute("item", studentService.fetchById(id));
        model.addAttribute("courses", studentService.studentCourses(id));
        return "student";
    }
}
