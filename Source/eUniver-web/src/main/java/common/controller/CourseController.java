package common.controller;

import common.controller.services.interfaces.CourseService;
import common.controller.services.interfaces.SimpleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import university.domain.Subject;

/**
 * Created by Albert Valiullin on 19.11.2016.
 */
@Controller
public class CourseController {
    @Autowired
    private CourseService courseService;

    @Autowired
    private SimpleService simpleService;

    @RequestMapping(value = "/course", method = RequestMethod.GET)
    public String getCourse(Model model) {
        model.addAttribute("list", courseService.fetchCourses());
        return "courses";
    }

    @RequestMapping(value = "/course/{id}", method = RequestMethod.GET)
    public String getCourse(Model model, @PathVariable("id") Long id) {
        model.addAttribute("item", courseService.fetchById(id));
        model.addAttribute("teachers", courseService.courseTeachers(id));
        model.addAttribute("list", courseService.courseStudents(id));
        return "course";
    }

    @RequestMapping(value = "/course/{id}/rating", method = RequestMethod.GET)
    public String getRating(Model model, @PathVariable("id") Long id) {
        model.addAttribute("item", courseService.fetchById(id));
        model.addAttribute("list", courseService.courseStudentsRating(id));
        return "courserating";
    }

    @RequestMapping(value = "/subject/{id}", method = RequestMethod.GET)
    public String getSubject(Model model, @PathVariable("id") Long id) {
        model.addAttribute("item", courseService.getAverageGradeForSubject(id));

        model.addAttribute("subjects", courseService.getAlsoChoosedSubjects(id, 3));
        return "subject";
    }

    @RequestMapping(value = "/subject", method = RequestMethod.GET)
    public String getSubjects(Model model) {
        model.addAttribute("list", simpleService.fetchAll(Subject.class));
        return "subjects";
    }


}
