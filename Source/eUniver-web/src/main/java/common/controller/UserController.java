package common.controller;

import common.controller.services.interfaces.AccountService;
import common.controller.services.interfaces.SimpleService;
import mobile.function.DomainObjectEditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import university.domain.*;

import javax.persistence.PostRemove;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by ПАХАРЬ on 05.11.2016.
 */
@Controller
public class UserController {

    @Autowired
    SimpleService simpleService;


    @Autowired
    AccountService accountService;



    @InitBinder
    public void initBinder(WebDataBinder binder){
        binder.registerCustomEditor(GenericSubject.class, new DomainObjectEditor(simpleService, GenericSubject.class) );
        binder.registerCustomEditor(Account.class, new DomainObjectEditor(simpleService, Account.class));
        binder.registerCustomEditor(AcademicGroup.class, new DomainObjectEditor(simpleService, AcademicGroup.class));
    }


    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String homePage(ModelMap model) {
        model.addAttribute("greeting", "Hi, Welcome to EUNIVER");
        return "welcome";
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String adminPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "admin";
    }

    @RequestMapping(value = "/access_denied", method = RequestMethod.GET)
    public String accessDeniedPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "accessDenied";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage( ) {
        return "login";
    }



    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }


    @RequestMapping(value = "/newStudent", method = RequestMethod.GET)
    public String newRegistrationStudent(ModelMap model) {
        Person student = new Student();
        model.addAttribute("student", student);
        return "newstudent";
    }

    @RequestMapping(value = "/newStudent", method = RequestMethod.POST)
    public String saveRegistrationStudent(@Valid Student student,
                                   BindingResult result, ModelMap model) {

        if (result.hasErrors()) {
            return "newstudent";
        }
        Long id = accountService.savePerson(student);
        model.addAttribute("success", "Student " + student.getFirstName() + " has been registered successfully");
        return "redirect:/newAccount/" + id;
    }

    @RequestMapping(value = "/newTeacher", method = RequestMethod.GET)
    public String newRegistrationTeacher(ModelMap model) {
        Person teacher = new Teacher();

        model.addAttribute("teacher", teacher);
        return "newteacher";
    }

    /*
     * This method will be called on form submission, handling POST request It
     * also validates the user input
     */
    @RequestMapping(value = "/newTeacher", method = RequestMethod.POST)
    public String saveRegistrationTeacher(@Valid Teacher teacher,
                                          BindingResult result, ModelMap model) {

        if (result.hasErrors()) {
            return "newteacher";
        }
        Long id = accountService.savePerson(teacher);
        model.addAttribute("success", "Teacher " + teacher.getFirstName() + " has been registered successfully");
        return "redirect:/newAccount/" + id;
    }

    @RequestMapping(value = "/newAccount/{id}", method = RequestMethod.GET)
    public String newRegistrationAccount(ModelMap model, @PathVariable("id") Long id) {

        Account account = new Account();
        model.addAttribute("account", account);
        Person person = accountService.findPersonById(id);
        model.addAttribute("success", model.get("success") + "<br>\n" +
                "        Would you like to create account ?\n" +
                "        <br/>");
        if (person.getType() == Person.Type.TEACHER) {
            model.addAttribute("roles", initializeProfiles(14));
        } else {
            model.addAttribute("roles", initializeProfiles(1));
        }
        return "newaccount";

    }

    @RequestMapping(value = "/newAccount/{id}", method = RequestMethod.POST)
    public String saveRegistrationAccount(@Valid Account account,
                                          BindingResult result, ModelMap model, @PathVariable("id") Long id) {

        if (result.hasErrors()) {
            Person person = accountService.findPersonById(id);
            if (person.getType() == Person.Type.TEACHER) {
                model.addAttribute("roles", initializeProfiles(14));
            } else {
                model.addAttribute("roles", initializeProfiles(1));
            }
            return "newaccount";
        }
        Person p = accountService.findPersonById(id);
        account.setPerson(p);
        accountService.saveAccount(account);

        model.addAttribute("success", "Teacher " + p.getFirstName() + " has been registered successfully");
        return "registrationsuccess";
    }

    private String getPrincipal(){
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            userName = ((UserDetails)principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }

    @ModelAttribute("groups")
    public List<AcademicGroup> initGroups(){

        return simpleService.fetchAll(AcademicGroup.class);
    }



    @ModelAttribute("personTypes")
    public List<Person.Type> initializePersons() {
        List<Person.Type> lst = new LinkedList<>();
        lst.add(Person.Type.STUDENT);
        lst.add(Person.Type.TEACHER);
        return lst;
    }

    @ModelAttribute("subjects")
    public List<GenericSubject> initializeSubjects() {
        return simpleService.fetchAll(GenericSubject.class);
    }

    private List<Account.Type> initializeProfiles(int m) {
        List<Account.Type> lst = new LinkedList<>();
        if(m >= 8){
            lst.add(Account.Type.ADMIN);
            m -= 8;
        }
        if(m >= 4){
            lst.add(Account.Type.DEPARTMENT_ADMIN);
            m -= 4;
        }
        if(m >= 2){
            lst.add(Account.Type.TEACHER);
            m -= 2;
        }
        if(m >= 1){
            lst.add(Account.Type.STUDENT);
        }
        return lst;
    }

}
