package common.controller;

import common.controller.services.interfaces.AccountService;
import common.controller.services.interfaces.SimpleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import university.common.SimpleDao;
import university.domain.*;

/**
 * Created by Damir on 04.07.16.
 */
@Controller
public class Dashboard {

    @Autowired
    private SimpleService simpleService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private SimpleDao simpleDao;

    @RequestMapping(value = "/alpha", method = RequestMethod.GET)
    public String root()
    {
        Account account = accountService.findByLogin("mainadmin");
        accountService.findPersonById(account.getPerson().getId());
        return "dashboard";
    }

    @RequestMapping(value = "/teacherass", method = RequestMethod.GET)
    @ResponseBody
    public String getTeachers() {
        return "hello";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home() {
        return "redirect:/home";
    }


}
