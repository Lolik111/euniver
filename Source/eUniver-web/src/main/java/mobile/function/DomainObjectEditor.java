package mobile.function;

import common.controller.domain.IdentifiedEntity;
import common.controller.domain.LongIdEntity;
import common.controller.services.interfaces.SimpleService;
import org.springframework.beans.SimpleTypeConverter;
import org.springframework.stereotype.Component;

import java.beans.PropertyEditorSupport;

/**
 * Created by ПАХАРЬ on 15.11.2016.
 */
@Component
public class DomainObjectEditor extends PropertyEditorSupport {

    private SimpleService simpleService;
    private Class<? extends IdentifiedEntity> type;

    private SimpleTypeConverter typeConverter = new SimpleTypeConverter();

    public DomainObjectEditor(SimpleService simpleService, Class<? extends IdentifiedEntity> type) {
        this.simpleService = simpleService;
        this.type = type;
    }

    @Override
    public String getAsText() {
        Object obj = getValue();
        if(obj == null) {
            return null;
        }

        if(obj instanceof LongIdEntity) {
            LongIdEntity domainObject = (LongIdEntity) obj;

            return typeConverter.convertIfNecessary(
                    domainObject.getId(), String.class);
        }

        throw new IllegalArgumentException("Value must be a DomainObject");
    }

    @Override
    public void setAsText(String text) {
        if(text == null || 0 == text.length()) {
            setValue(null);
            return;
        }
        Long l = typeConverter.convertIfNecessary(text, Long.class);
        Object some = simpleService.findById(type, l);
        setValue(some);
    }

}
