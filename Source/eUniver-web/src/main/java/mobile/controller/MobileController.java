package mobile.controller;

import common.controller.services.interfaces.AccountService;
import common.controller.services.interfaces.ScheduleService;
import common.controller.services.interfaces.SimpleService;
import common.model.PersonDto;
import common.model.StudentDto;
import common.model.TeacherDto;
import mobile.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.context.SecurityContextHolder;
import university.domain.Account;
import university.domain.Person;

import java.util.*;

/**
 * Created by Ильнар on 02.08.2016.
 */
@RestController
public class MobileController {
    private final static long DEFAULT_CLASS_DURATION = (long) (1000 * 60 * 60 * 1.5);

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private AccountService accountService;


    @RequestMapping(value = "/api/login", method = RequestMethod.POST)
    public LoginResponse login()
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(auth instanceof AnonymousAuthenticationToken) {
            return null;
        }
        String username = auth.getName();
        Account account = accountService.findAccWithPersonByLogin(username);
        if(account == null){
            throw new UsernameNotFoundException("Username not found");
        }
        accountService.addAttemptLogIn(username);
        if(!accountService.checkAllowedLogIn(username, 5)){
            throw new UsernameNotFoundException("Username not allowed");
        }

        Person person = account.getPerson();

        LoginResponse loginResponse = new LoginResponse();
        PersonDto personDto;
        if(person.getType() == Person.Type.STUDENT){
            personDto = new StudentDto();
            String details = accountService.getStudentDetails(person.getId());
            ((StudentDto)personDto).setDetails(details);
        } else {
            personDto = new TeacherDto();
        }

        personDto.setId(person.getId());
        personDto.setFirstName(person.getFirstName());
        personDto.setSecondName(person.getSecondName());
        personDto.setPatronymic(person.getPatronymic());
        personDto.setImageUrl(person.getImageUrl());
        personDto.setType(person.getType());
        loginResponse.setUser(personDto);
        loginResponse.setToken("ABC");

        return loginResponse;
    }

    @RequestMapping(value = "/api/class_schedule", method = RequestMethod.GET)
    public ClassSchedule getClassSchedule(@RequestParam("from") long from,
                                          @RequestParam("to") long to) {
        Date fromDate = new Date(from);
        Date toDate = new Date(to);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(auth instanceof AnonymousAuthenticationToken) {
            return null;
        }
        String username = auth.getName();
        ClassSchedule classSchedule = scheduleService.getSchedule(fromDate, toDate, username);
        classSchedule.setClassDuration(DEFAULT_CLASS_DURATION);

        return classSchedule;
    }

}
