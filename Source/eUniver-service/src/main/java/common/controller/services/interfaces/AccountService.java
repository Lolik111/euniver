package common.controller.services.interfaces;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import university.domain.Account;
import university.domain.Person;

/**
 * Created by ПАХАРЬ on 05.11.2016.
 */
public interface AccountService {

    Account findByLogin(String login);

    Account findAccWithPersonByLogin(String login);

    int addAttemptLogIn(String login);

    @Transactional(readOnly = true)
    boolean checkAllowedLogIn(String login, int times);

    int getAttemptsLogIn(String login);

    Long saveAccount(Account account);

    Long savePerson(Person person);

    Person findPersonById(Long id);

    Long findAccountIdByPersonId(Long id);

    String getStudentDetails(Long id);
}
