package common.controller.services;

import common.controller.services.interfaces.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import university.common.SimpleDao;

import java.util.List;
import java.util.Map;

/**
 * Created by Albert Valiullin on 19.11.2016.
 */
@Service
public class GroupServiceImpl implements GroupService {
    @Autowired
    private SimpleDao simpleDao;


    @Override
    public List<Map<String, Object>> fetchGroups() {
        String query = "SELECT   academic_group_id, ag.name academic_group_name, department_id, dp.name department_name, count(student_id) student_count\n" +
                "FROM     academic_group ag\n" +
                "         join academic_flow on academic_flow_id = academic_flow_ref\n" +
                "         join department dp on department_id = department_ref\n" +
                "         left join student on academic_group_id = academic_group_ref\n" +
                "GROUP BY academic_group_id, ag.name, department_id, dp.name\n" +
                "ORDER BY department_name, academic_group_name";
        return simpleDao.execQueryMapResult(query);
    }

    @Override
    public Map<String, Object> fetchById(Long id) {
        String query = "SELECT   academic_group_id, ag.name academic_group_name, department_id, dp.name department_name\n" +
                "FROM     (SELECT * FROM academic_group WHERE academic_group_id = " + id + ") ag\n" +
                "         join academic_flow on academic_flow_id = academic_flow_ref\n" +
                "         join department dp on department_id = department_ref";
        return simpleDao.execQueryMapSingleResult(query);
    }

    @Override
    public List<Map<String, Object>> groupStudents(Long id) {
        String query = "SELECT   student_id, first_name, second_name, patronymic, image_url\n" +
                "FROM     student\n" +
                "         join person on student_id = person_id\n" +
                "WHERE    academic_group_ref = " + id + "\n" +
                "ORDER BY second_name, first_name, patronymic";
        return simpleDao.execQueryMapResult(query);
    }

    @Override
    public List<Map<String, Object>> groupStudentsRating(Long id) {
        String query = "SELECT   student_id, first_name, second_name, patronymic, image_url, avg(avg_grade) grade\n" +
                "FROM     (SELECT   student_id, course_id, avg(grade) avg_grade\n" +
                "          FROM     (SELECT * FROM student WHERE academic_group_ref = " + id + ") s\n" +
                "                   join student_visitor on student_id = student_ref\n" +
                "                   join class_plan on class_plan_id = visitors_group_ref\n" +
                "                   join course on course_id = course_ref\n" +
                "                   join subject sub on subject_id = subject_ref\n" +
                "                   left join assignment on class_plan_id = class_plan_ref\n" +
                "                   left join assignment_grade ag on assignment_id = ag.assignment_ref and student_id = ag.student_ref\n" +
                "          GROUP BY student_id, course_id) grades\n" +
                "          join person on student_id = person_id\n" +
                "GROUP BY student_id, first_name, second_name, patronymic, image_url\n" +
                "ORDER BY grade DESC NULLS LAST";
        return simpleDao.execQueryMapResult(query);
    }
}
