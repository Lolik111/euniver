package common.controller.services.security;

import common.controller.services.interfaces.AccountService;
import common.controller.services.interfaces.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by ПАХАРЬ on 07.11.2016.
 */
public class AuthenticationFailureHandlerImpl implements AuthenticationFailureHandler {

    @Autowired
    private AccountService userService;

    @Autowired
    private PropertyService propertyService;

    @Override
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {

        RedirectStrategy strategy = new DefaultRedirectStrategy();

        String username = httpServletRequest.getParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY);
        if(userService.findByLogin(username) != null){
            propertyService.incrementLogIn();
            int val = userService.addAttemptLogIn(username);
            if(val > 4){
                strategy.sendRedirect(httpServletRequest, httpServletResponse, "/login?mercy");
                return;
            }


        }
        strategy.sendRedirect(httpServletRequest, httpServletResponse, "/login?error");

    }
}
