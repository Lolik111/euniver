package common.controller.services.security;

import common.controller.services.interfaces.AccountService;
import common.controller.services.interfaces.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by ПАХАРЬ on 07.11.2016.
 */
public class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler {

    @Autowired
    private AccountService userService;

    @Autowired
    private PropertyService propertyService;

    @Override
    @Transactional
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        String username = authentication.getName();
        userService.addAttemptLogIn(username);
        propertyService.incrementLogIn();
        propertyService.incrementSuccessfulLogIn();

        RedirectStrategy strategy = new DefaultRedirectStrategy();
        strategy.sendRedirect(httpServletRequest, httpServletResponse, "/home");


    }
}
