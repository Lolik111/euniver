package common.controller.services.interfaces;

import java.util.List;
import java.util.Map;

/**
 * Created by Albert Valiullin on 19.11.2016.
 */

public interface CourseService {

    List<Map<String, Object>> fetchCourses();

    Map<String, Object> fetchById(Long id);

    List<Map<String, Object>> courseStudents(Long id);

    List<Map<String, Object>> courseTeachers(Long id);

    List<Map<String, Object>> courseStudentsRating(Long id);

    Map<String, Object> getAverageGradeForSubject(Long subjectId);

    List<Map<String, Object>> getAlsoChoosedSubjects(Long subjectId, int firstN);

}
