package common.controller.services;

import common.controller.services.interfaces.CourseService;
import common.controller.services.interfaces.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import university.common.SimpleDao;

import java.util.List;
import java.util.Map;

/**
 * Created by Albert Valiullin on 19.11.2016.
 */
@Service
public class CourseServiceImpl implements CourseService {
    @Autowired
    private SimpleDao simpleDao;


    @Override
    public List<Map<String, Object>> fetchCourses() {
        String query = "SELECT   course_id, subject_id, name subject_name, academic_year, semester, teacher_count, student_count\n" +
                "FROM     course\n" +
                "         join subject on subject_id = subject_ref\n" +
                "         join (SELECT   course_ref, count(DISTINCT teacher_ref) teacher_count\n" +
                "               FROM     class_plan\n" +
                "               GROUP BY course_ref) t on course_id = t.course_ref\n" +
                "         join (SELECT   course_ref, count(DISTINCT student_id) student_count\n" +
                "               FROM     class_plan\n" +
                "                        join student_visitor on visitors_group_ref = class_plan_id\n" +
                "                        join student on student_id = student_ref\n" +
                "               GROUP BY course_ref) s on course_id = s.course_ref\n" +
                "ORDER BY academic_year DESC, name ASC";
        return simpleDao.execQueryMapResult(query);
    }

    @Override
    public Map<String, Object> fetchById(Long id) {
        String query = "SELECT   subject_id, name subject_name, academic_year, semester, count(DISTINCT teacher_ref) teacher_count\n" +
                "FROM     (SELECT * FROM course WHERE course_id = " + id + ") c\n" +
                "         join subject on subject_id = subject_ref\n" +
                "         join class_plan on course_id = course_ref\n" +
                "         left join person on person_id = teacher_ref\n" +
                "GROUP BY subject_id, name, academic_year, semester";
        return simpleDao.execQueryMapSingleResult(query);
    }

    @Override
    public List<Map<String, Object>> courseStudents(Long id) {
        String query = "SELECT DISTINCT student_id, first_name, second_name, patronymic, image_url, academic_group_id, name group_name\n" +
                "FROM     (SELECT * FROM class_plan WHERE course_ref = " + id + ") c\n" +
                "         join student_visitor on visitors_group_ref = class_plan_id\n" +
                "         join student on student_id = student_ref\n" +
                "         join person on person_id = student_id\n" +
                "         join academic_group on academic_group_ref = academic_group_id\n" +
                "ORDER BY second_name, first_name, patronymic";
        return simpleDao.execQueryMapResult(query);
    }

    @Override
    public List<Map<String, Object>> courseTeachers(Long id) {
        String query = "SELECT   person_id teacher_id, first_name, second_name, patronymic, image_url, string_agg(DISTINCT c.type, ', ') \"types\"\n" +
                "FROM     (SELECT * FROM class_plan WHERE course_ref = " + id + ") c\n" +
                "         join person on person_id = teacher_ref\n" +
                "GROUP BY teacher_id, first_name, second_name, patronymic, image_url\n" +
                "ORDER BY second_name, first_name, patronymic";
        return simpleDao.execQueryMapResult(query);
    }

    @Override
    public List<Map<String, Object>>courseStudentsRating(Long id) {
        String query = "SELECT   student_id, first_name, second_name, patronymic, image_url, academic_group_id, name group_name, avg(grade) grade\n" +
                "FROM     student\n" +
                "         join student_visitor on student_id = student_ref\n" +
                "         join class_plan on class_plan_id = visitors_group_ref\n" +
                "         left join assignment on class_plan_id = class_plan_ref\n" +
                "         left join assignment_grade ag on assignment_id = ag.assignment_ref and student_id = ag.student_ref\n" +
                "         join person on student_id = person_id\n" +
                "         join academic_group on academic_group_ref = academic_group_id\n" +
                "WHERE    course_ref = " + id + "\n" +
                "GROUP BY student_id, first_name, second_name, patronymic, image_url, academic_group_id, name\n" +
                "ORDER BY grade DESC NULLS LAST";
        return simpleDao.execQueryMapResult(query);
    }

    @Override
    public Map<String, Object> getAverageGradeForSubject(Long subjectId) {
        String query = String.format("SELECT t1.subject_id, t1.name as subject_name, AVG(t5.grade) as avg_grade\n" +
                "FROM subject t1\n" +
                "JOIN course t2 ON t2.subject_ref = t1.subject_id\n" +
                "JOIN class_plan t3 ON t3.course_ref = t2.course_id\n" +
                "JOIN assignment t4 ON t4.class_plan_ref = t3.class_plan_id\n" +
                "JOIN assignment_grade t5 ON t5.assignment_ref = t4.assignment_id AND t5.visitors_group_ref = t4.class_plan_ref\n" +
                "WHERE subject_id = %s\n" +
                "GROUP BY subject_id, subject_name  LIMIT 1", subjectId);
        return simpleDao.execQueryMapSingleResult(query);
    }

    @Override
    public List<Map<String, Object>> getAlsoChoosedSubjects(Long subjectId, int firstN) {
        String query = String.format("\n" +
                "SELECT t8.subject_id, t8.name as subject_name, count(*) as cc\n" +
                "FROM\n" +
                "(SELECT DISTINCT student_ref\n" +
                "FROM subject t1\n" +
                "JOIN course t2 ON t2.subject_ref = t1.subject_id\n" +
                "JOIN class_plan t3 ON t3.course_ref = t2.course_id\n" +
                "JOIN student_visitor t4 ON t4.visitors_group_ref = t3.class_plan_id\n" +
                "WHERE subject_id = %s \n" +
                ") ts\n" +
                "JOIN student_visitor t5 ON ts.student_ref = t5.student_ref\n" +
                "JOIN class_plan t6 ON t6.class_plan_id = t5.visitors_group_ref\n" +
                "JOIN course t7 ON t7.course_id = t6.course_ref\n" +
                "JOIN subject t8 ON t8.subject_id = t7.subject_ref\n" +
                "WHERE subject_id <> %s\n" +
                "GROUP BY subject_id, subject_name \n" +
                "ORDER BY cc DESC LIMIT %s", subjectId, subjectId, firstN);
        return simpleDao.execQueryMapResult(query);
    }
}
