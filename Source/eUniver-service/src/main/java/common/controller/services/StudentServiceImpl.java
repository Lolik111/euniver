package common.controller.services;

import common.controller.services.interfaces.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import university.common.SimpleDao;

import java.util.List;
import java.util.Map;

/**
 * Created by Albert Valiullin on 18.11.2016.
 */
@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private SimpleDao simpleDao;

    public List<Map<String, Object>> fetchStudentsGroups() {
        String query = "SELECT   student_id, first_name, second_name, patronymic, image_url, academic_group_id, name\n" +
                "FROM     student\n" +
                "         join person on student_id = person_id\n" +
                "         join academic_group on academic_group_ref = academic_group_id\n" +
                "ORDER BY second_name, first_name, patronymic";
        return simpleDao.execQueryMapResult(query);
    }

    @Override
    public List<Map<String, Object>>  fetchStudentVisitors(Long id) {
        String query = String.format("SELECT student_id, first_name, second_name, patronymic, image_url, academic_group_id, name\n" +
                "FROM student\n" +
                "JOIN person ON student_id = person_id\n" +
                "JOIN academic_group ON academic_group_ref = academic_group_id\n" +
                "JOIN student_visitor ON student_id = student_ref" +
                "WHERE visitors_group_ref = %s " +
                "ORDER BY second_name, first_name, patronymic", id);
        return simpleDao.execQueryMapResult(query);
    }

    @Override
    public Map<String, Object> fetchById(Long id) {
        String query = "SELECT   student_id, first_name, second_name, patronymic, image_url, academic_group_id, name\n" +
                "FROM     (SELECT * FROM student WHERE student_id = " + id + ") s\n" +
                "         join person on student_id = person_id\n" +
                "         join academic_group on academic_group_ref = academic_group_id";
        return simpleDao.execQueryMapSingleResult(query);
    }

    @Override
    public List<Map<String, Object>> studentCourses(Long id) {
        String query = "SELECT   course_id, sub.name subject_name, academic_year, avg(grade) avg_grade\n" +
                "FROM     (SELECT * FROM student WHERE student_id = " + id + ") s\n" +
                "         join student_visitor on student_id = student_ref\n" +
                "         join class_plan on class_plan_id = visitors_group_ref\n" +
                "         join course on course_id = course_ref\n" +
                "         join subject sub on subject_id = subject_ref\n" +
                "         left join assignment on class_plan_id = class_plan_ref\n" +
                "         left join assignment_grade ag on assignment_id = ag.assignment_ref and student_id = ag.student_ref\n" +
                "GROUP BY student_id, course_id, sub.name, academic_year";
        return simpleDao.execQueryMapResult(query);
    }
}
