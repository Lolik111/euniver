package common.controller.services.interfaces;

import common.controller.domain.IdentifiedEntity;
import common.model.BaseDto;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.function.Function;

/**
 * Created by Damir on 04.07.16.
 */
public interface SimpleService {

    <D> D findById(Class<D> aClass, Serializable aId);

    <D> List<D> fetchAll(Class<D> aClass);

}
