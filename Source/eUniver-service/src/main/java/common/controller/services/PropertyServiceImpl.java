package common.controller.services;

import common.controller.services.interfaces.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import university.common.SimpleDao;
import university.domain.AppProperty;


/**
 * Created by ПАХАРЬ on 06.11.2016.
 */
@Service("PropertyService")
public class PropertyServiceImpl implements PropertyService {

    @Autowired
    SimpleDao simpleDao;

    @Override
    @Transactional
    public void incrementLogIn() {
        AppProperty logInProperty = simpleDao.findByField(AppProperty.class, "name", "log_in");
        logInProperty.incrementValue();
        simpleDao.saveOrUpdate(logInProperty);

    }

    @Override
    @Transactional
    public void incrementSuccessfulLogIn() {
        AppProperty logInProperty = simpleDao.findByField(AppProperty.class, "name", "s_log_in");
        logInProperty.incrementValue();
        simpleDao.saveOrUpdate(logInProperty);
    }

    @Override
    public int getAverage() {
        AppProperty logInProperty = simpleDao.findByField(AppProperty.class, "name", "log_in");
        AppProperty slogInProperty = simpleDao.findByField(AppProperty.class, "name", "s_log_in");
        int logsIn = Integer.parseInt(logInProperty.getValue()) + 1;
        int slogsIn = Integer.parseInt(slogInProperty.getValue());
        return (logsIn/slogsIn) + 1;
    }
}
