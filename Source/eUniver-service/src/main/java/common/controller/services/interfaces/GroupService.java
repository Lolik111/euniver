package common.controller.services.interfaces;

import java.util.List;
import java.util.Map;

/**
 * Created by Albert Valiullin on 19.11.2016.
 */
public interface GroupService {
    List<Map<String, Object>> fetchGroups();
    Map<String, Object> fetchById(Long id);
    List<Map<String, Object>> groupStudents(Long id);

    List<Map<String, Object>> groupStudentsRating(Long id);
}
