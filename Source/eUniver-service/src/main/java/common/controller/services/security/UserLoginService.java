package common.controller.services.security;


import common.controller.services.interfaces.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import university.domain.Account;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by ПАХАРЬ on 05.11.2016.
 */
@Service("UserLoginService")
public class UserLoginService implements UserDetailsService {

    private static final int MAGIC_ATTEMPTS_CONSTANT = 5;

    @Autowired
    private AccountService userService;

    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account user = userService.findByLogin(username);
        if(user == null){
            throw new UsernameNotFoundException("Username not found");
        }
        if(!userService.checkAllowedLogIn(username, MAGIC_ATTEMPTS_CONSTANT)){
            throw new UsernameNotFoundException("Username not allowed");
        }

        List<GrantedAuthority> authorities = new LinkedList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_" + user.getType()));
        return new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPassword(),
                true, true, true, true, authorities);
    }




}