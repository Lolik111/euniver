package common.controller.services;

import common.controller.services.interfaces.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import university.common.SimpleDao;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Ильнар on 02.08.2016.
 */
@Service("teacherService")
public class TeacherServiceImpl implements TeacherService {

    @Autowired
    private SimpleDao simpleDao;


    @Override
    public List<Map<String, Object>> fetchTeachersClassPlans(Long id) {
        String query = String.format("SELECT class_plan_id, t4.name as subject_name, academic_year, type, tag\n" +
                "FROM teacher t1 \n" +
                "JOIN class_plan t2 on t2.teacher_ref = t1.teacher_id \n" +
                "JOIN course t3 on t3.course_id = t2.course_ref \n" +
                "JOIN subject t4 on t4.subject_id = t3.subject_ref\n" +
                "WHERE teacher_id = %s", id);
        return simpleDao.execQueryMapResult(query);
    }

    @Override
    public List<Long> fetchTeachersClassPlanIds(Long id) {
        String query = String.format("SELECT class_plan_id \n" +
                "FROM teacher t1 \n" +
                "JOIN class_plan t2 on t2.teacher_ref = t1.teacher_id \n" +
                "WHERE teacher_id = %s", id);

        List<BigInteger> result = simpleDao.execQuery(query);
        List<Long> ans = new ArrayList<>();
        for (BigInteger integer : result ) {
            ans.add(integer.longValue());
        }

        return ans;
    }

    @Override
    public Map<String, Object> fetchSingleClassPlan(Long classPlanId) {
        String query = String.format("SELECT class_plan_id, subject_id, t4.name as subject_name, academic_year, type, tag\n" +
                "FROM class_plan t2 \n" +
                "JOIN course t3 on t3.course_id = t2.course_ref \n" +
                "JOIN subject t4 on t4.subject_id = t3.subject_ref\n" +
                "WHERE class_plan_id = %s \n" +
                "LIMIT 1", classPlanId);
        return simpleDao.execQueryMapResult(query).get(0);
    }

    @Override
    public List<Map<String, Object>> fetchClassesInfo(Long classPlanId) {
        String query = String.format("SELECT class_id, date, time, t4.number as class_number, room_id, t5.number as room_number, t6.name as building_name  \n" +
                "FROM class_plan t1 \n" +
                "JOIN class t4 ON t4.class_plan_ref = t1.class_plan_id\n" +
                "JOIN room t5 ON t5.room_id = t4.room_ref\n" +
                "JOIN building t6 ON t6.building_id = t5.building_ref\n" +
                "WHERE class_plan_id = %s \n", classPlanId);
        return simpleDao.execQueryMapResult(query);

    }

    @Override
    public List<Map<String, Object>> fetchAssignmentsInfo(Long classPlanId) {
        String query = String.format("SELECT assignment_id, creation_date, materials_url, description \n" +
                "FROM class_plan t1 \n" +
                "JOIN assignment t2 ON t1.class_plan_id = t2.class_plan_ref \n" +
                "WHERE class_plan_id = %s \n", classPlanId);
        return simpleDao.execQueryMapResult(query);
    }

    @Override
    public Long getClassPlanByAssignmentId(Long assignmentId) {
        String query = String.format("SELECT class_plan_ref FROM assignment WHERE assignment_id = %s LIMIT 1", assignmentId);
        return ((BigInteger)simpleDao.execQuery(query).get(0)).longValue();
    }


    @Override
    public Map<String,Object> fetchAssignment(Long id){
        String query = String.format("SELECT assignment_id, creation_date, materials_url, description \n" +
                "FROM assignment\n" +
                "WHERE assignment_id = %s  LIMIT 1", id);
        return simpleDao.execQueryMapResult(query).get(0);
    }


    @Override
    public List<Map<String,Object>> fetchGradesByAssignment(Long id){
        addNonExistedAssignmentGrades(id);
        String query = String.format("SELECT (ROW_NUMBER() OVER (ORDER BY student_id ASC) - 1) as row_id, student_id, first_name, second_name, patronymic, image_url,\n" +
                "academic_group_id, t6.name as group_name, t7.grade\n" +
                "FROM assignment t1\n" +
                "JOIN class_plan t2 ON t1.class_plan_ref = t2.class_plan_id\n" +
                "JOIN student_visitor t3 ON t3.visitors_group_ref = t2.class_plan_id\n" +
                "JOIN student t4 ON t4.student_id = t3.student_ref\n" +
                "JOIN person t5 ON t5.person_id = t4.student_id\n" +
                "JOIN academic_group t6 ON t4.academic_group_ref = t6.academic_group_id\n" +
                "LEFT JOIN assignment_grade t7 ON t1.class_plan_ref = t7.visitors_group_ref AND t7.assignment_ref = t1.assignment_id AND t4.student_id = t7.student_ref\n" +
                "WHERE assignment_id = %s ", id);

        return simpleDao.execQueryMapResult(query);
    }

    @Override
    public void updateGradeByAssignment(Long studentId, Long assignmentId, Integer grade) {
        String query = String.format("UPDATE assignment_grade\n" +
                "SET grade = %s\n" +
                "WHERE assignment_ref = %s AND student_ref = %s", grade, assignmentId, studentId);
        simpleDao.execSqlUpdateQuery(query);
    }

    /**
     *
     * @param assignmentId
     * @return true, if everything is good, false if we need additional inserts
     */
    @Override
    public boolean assignmentGradesExistanceCheck(Long assignmentId) {
        String query = String.format("SELECT COUNT(*) as cc \n" +
                "FROM \n" +
                "(SELECT t1.*\n" +
                "FROM student_visitor t1 \n" +
                "LEFT JOIN assignment_grade t2 ON t1.student_ref = t2.student_ref AND t1.visitors_group_ref = t2.visitors_group_ref \n" +
                "WHERE assignment_ref = %s\n" +
                "EXCEPT\n" +
                "SELECT t1.*\n" +
                "FROM student_visitor t1 \n" +
                "JOIN assignment_grade t2 ON t1.student_ref = t2.student_ref AND t1.visitors_group_ref = t2.visitors_group_ref \n" +
                "WHERE assignment_ref = %s) t4", assignmentId, assignmentId);
        BigInteger bi = (BigInteger) simpleDao.execQuery(query).get(0);
        return bi.longValue() == 0;

    }

    @Override
    public void addNonExistedAssignmentGrades(Long assignmentId) {
        if(this.assignmentGradesExistanceCheck(assignmentId)){
            return;
        }
        String query = String.format("INSERT INTO assignment_grade (visitors_group_ref, student_ref, assignment_ref, grade)\n" +
                "SELECT t4.visitors_group_ref, t4.student_ref, %s, NULL\n" +
                "FROM\n" +
                "(SELECT t1.*\n" +
                "FROM student_visitor t1 \n" +
                "LEFT JOIN assignment_grade t2 ON t1.student_ref = t2.student_ref AND t1.visitors_group_ref = t2.visitors_group_ref\n" +
                "EXCEPT\n" +
                "SELECT t1.*\n" +
                "FROM student_visitor t1 \n" +
                "JOIN assignment_grade t2 ON t1.student_ref = t2.student_ref AND t1.visitors_group_ref = t2.visitors_group_ref) t4", assignmentId);
        simpleDao.execSqlUpdateQuery(query);
    }

    @Override
    public List<Map<String, Object>> fetchSmoothedGradesForPlanByTeacher(Long teacherID, int rateOfStupidness) {
        String query = String.format("\n" +
                "SELECT class_plan_id, AVG(grade) as avg_grade\n" +
                "FROM class_plan t1\n" +
                "JOIN assignment t2 ON t1.class_plan_id = t2.class_plan_ref\n" +
                "LEFT JOIN assignment_grade t3 ON t2.assignment_id = t3.assignment_ref AND t2.class_plan_ref = t3.visitors_group_ref\n" +
                "WHERE NOT EXISTS \n" +
                "   (SELECT * \n" +
                "    FROM\n" +
                "        (--stupid students \n" +
                "        SELECT t6.student_ref\n" +
                "        FROM\n" +
                "        (SELECT t5.student_ref, t3.class_plan_id, avg(grade) as avg_for_class_plan\n" +
                "        FROM\n" +
                "        class_plan t3 \n" +
                "        JOIN assignment t4 ON t4.class_plan_ref = t3.class_plan_id\n" +
                "        JOIN assignment_grade t5 ON t5.assignment_ref = t4.assignment_id\n" +
                "        WHERE grade is not null\n" +
                "        GROUP by t5.student_ref, class_plan_id) t6\n" +
                "        GROUP BY student_ref\n" +
                "        HAVING AVG(t6.avg_for_class_plan) < %s) t7\n" +
                "    WHERE t3.student_ref = t7.student_ref\n" +
                "    ) AND t1.teacher_ref = %s \n" +
                "GROUP by class_plan_id\n", rateOfStupidness, teacherID);
        return simpleDao.execQueryMapResult(query);
    }


}
