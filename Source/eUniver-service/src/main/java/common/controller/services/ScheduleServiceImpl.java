package common.controller.services;

import common.controller.services.interfaces.AccountService;
import common.controller.services.interfaces.ScheduleService;
import common.model.*;
import mobile.model.ClassSchedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import university.common.SimpleDao;
import university.domain.Account;
import university.domain.Person;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ПАХАРЬ on 13.11.2016.
 */
@Service("ScheduleService")
public class ScheduleServiceImpl implements ScheduleService {

    @Autowired
    private SimpleDao simpleDao;

    @Autowired
    private AccountService accountService;


    @Override
    public ClassSchedule getSchedule(Date from, Date to, String username) {
        Account account = accountService.findAccWithPersonByLogin(username);
        Person p = account.getPerson();  //simpleDao.findById(Person.class, account.getId());
        String sqlQuery;
        if (p.getType() == Person.Type.STUDENT) {
            sqlQuery = String.format(
                    "SELECT t3.class_id, t3.date, t3.time, t3.number, " +
                            "t2.type, " +
                            "t4.room_id, t4.number as room_number, " +
                            "t9.building_id, t9.name as building_name, " +
                            "t6.subject_id, t6.name as subject_name, " +
                            "t5.academic_year, t5.semester, " +
                            "t10.generic_subject_id, t10.name, \n" +
                            "t7.teacher_id, t8.first_name, t8.second_name \n" +
                            "FROM student_visitor t1 " +
                            "JOIN class_plan t2 ON t1.visitors_group_ref = t2.class_plan_id \n" +
                            "JOIN class t3 ON t3.class_plan_ref = t2.class_plan_id \n" +
                            "JOIN room t4 ON t4.room_id = t3.room_ref \n" +
                            "JOIN building t9 ON t4.building_ref = t9.building_id \n" +
                            "JOIN course t5 ON t2.course_ref = t5.course_id \n" +
                            "JOIN subject t6 ON t5.subject_ref = t6.subject_id \n" +
                            "JOIN teacher t7 ON t2.teacher_ref = t7.teacher_id \n" +
                            "JOIN person t8 ON t7.teacher_id = t8.person_id \n" +
                            "JOIN generic_subject t10 ON t6.generic_subject_ref = t10.generic_subject_id \n" +
                            "WHERE t3.date >= '%s' AND t3.date <= '%s' AND t1.student_ref = %s ", new java.sql.Date(from.getTime()),
                    new java.sql.Date(to.getTime()), p.getId());
        } else {
            sqlQuery = String.format("SELECT t3.class_id, t3.date, t3.time, t3.number, t2.type, " +
                            "t4.room_id, t4.number as room_number," +
                            " t9.building_id, t9.name as building_name, \n" +
                            "t6.subject_id, t6.name as subject_name, " +
                            "t5.academic_year, t5.semester," +
                            " t10.generic_subject_id, t10.name\n" +
                            "FROM teacher t1 JOIN class_plan t2 ON t1.teacher_id = t2.teacher_ref\n" +
                            "JOIN class t3 ON t3.class_plan_ref = t2.class_plan_id \n" +
                            "JOIN room t4 ON t4.room_id = t3.room_ref \n" +
                            "JOIN building t9 ON t4.building_ref = t9.building_id \n" +
                            "JOIN course t5 ON t2.course_ref = t5.course_id \n" +
                            "JOIN subject t6 ON t5.subject_ref = t6.subject_id \n" +
                            "JOIN generic_subject t10 ON t6.generic_subject_ref = t10.generic_subject_id \n" +
                            "WHERE t3.date >= '%s' AND t3.date <= '%s' AND t1.teacher_id = %s", new java.sql.Date(from.getTime()),
                    new java.sql.Date(to.getTime()), p.getId());
        }

        List<Object[]> classes = simpleDao.execQuery(sqlQuery);
        ClassSchedule schedule = new ClassSchedule();
        List<ClassDto> classDtos = new ArrayList<>();
        for (Object[] data : classes) {
            ClassDto classDto;
            if (p.getType() == Person.Type.STUDENT) {
                classDto = new StudentClassDto();
                TeacherDto teacherDto = new TeacherDto();
                teacherDto.setId(((BigInteger) data[15]).longValue());
                teacherDto.setFirstName((String) data[16]);
                teacherDto.setSecondName((String) data[17]);
                ((StudentClassDto) classDto).setTeacherDto(teacherDto);
            } else {
                classDto = new TeacherClassDto();
            }

            classDto.setId(((BigInteger) data[0]).longValue());
            classDto.setDate((Date) data[1]);
//            classDto.setTime((Date)data[2]);
            classDto.setNumber((Integer) data[3]);
            classDto.setType((String) data[4]);
            RoomDto room = new RoomDto();
            room.setId(((BigInteger) data[5]).longValue());
            room.setNumber((String) data[6]);
            BuildingDto building = new BuildingDto();
            building.setId(((BigInteger) data[7]).longValue());
            building.setName((String) data[8]);
            room.setBuilding(building);

            SubjectDto subject = new SubjectDto();
            GenericSubjectDto genericSubjectDto = new GenericSubjectDto();
            subject.setId(((BigInteger) data[9]).longValue());
            subject.setName((String) (data[10] + " " + data[11] + "/" + data[12]));
            genericSubjectDto.setId(((BigInteger) data[13]).longValue());
            genericSubjectDto.setName((String) data[14]);
            subject.setGenericSubject(genericSubjectDto);

            classDto.setRoom(room);
            classDto.setSubject(subject);
            classDtos.add(classDto);
        }
        schedule.setSchedule(classDtos);
        return schedule;

    }
}
