package common.controller.services.interfaces;

import common.controller.domain.IdentifiedEntity;
import common.model.FacultyDto;
import common.model.TeacherDto;
import org.omg.CORBA.portable.IDLEntity;
import org.springframework.transaction.annotation.Transactional;
import university.domain.Teacher;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by Ильнар on 02.08.2016.
 */
public interface TeacherService {

    List<Map<String, Object>> fetchTeachersClassPlans(Long id);

    List<Long> fetchTeachersClassPlanIds(Long id);

    Map<String, Object> fetchSingleClassPlan(Long classPlanId);

    List<Map<String, Object>> fetchClassesInfo(Long classPlanId);

    List<Map<String, Object>> fetchAssignmentsInfo(Long classPlanId);

    Long getClassPlanByAssignmentId(Long assignmentId);

    Map<String,Object> fetchAssignment(Long id);

    List<Map<String,Object>> fetchGradesByAssignment(Long id);

    void updateGradeByAssignment(Long studentId, Long assignmentId, Integer grade);

    boolean assignmentGradesExistanceCheck(Long assignmentId);

    void addNonExistedAssignmentGrades(Long assignmentId);

    List<Map<String, Object>> fetchSmoothedGradesForPlanByTeacher(Long teacherID, int rateOfStupidness);

}
