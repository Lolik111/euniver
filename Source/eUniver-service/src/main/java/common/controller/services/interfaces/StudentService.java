package common.controller.services.interfaces;

import java.util.List;
import java.util.Map;

/**
 * Created by Albert Valiullin on 18.11.2016.
 */

public interface StudentService {
    List<Map<String, Object>> fetchStudentsGroups();

    List<Map<String, Object>> fetchStudentVisitors(Long id);

    Map<String, Object> fetchById(Long id);

    List<Map<String, Object>> studentCourses(Long id);
}
