package common.controller.services.interfaces;

/**
 * Created by ПАХАРЬ on 06.11.2016.
 */
public interface PropertyService {

    void incrementLogIn();

    void incrementSuccessfulLogIn();

    int getAverage();
}
