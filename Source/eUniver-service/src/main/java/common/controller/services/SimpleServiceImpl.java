package common.controller.services;

import common.controller.domain.IdentifiedEntity;
import common.controller.services.interfaces.SimpleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import university.common.SimpleDao;

import java.io.Serializable;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by Damir on 04.07.16.
 */
@Service("simpleService")
public class SimpleServiceImpl implements SimpleService {

    @Autowired
    private SimpleDao simpleDao;

    @Override
    @Transactional(readOnly = true)
    public <D> D findById(Class<D> aClass, Serializable aId) {
        return simpleDao.findById(aClass, aId);
    }


    @Override
    @Transactional(readOnly = true)
    public <D> List<D> fetchAll(final Class<D> aClass) {
        return simpleDao.fetchAll(aClass);
    }

}
