package common.controller.services;

import common.controller.services.interfaces.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import university.common.SimpleDao;
import university.domain.*;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by ПАХАРЬ on 05.11.2016.
 */
@Service("AccountService")
public class AccountServiceImpl implements AccountService {

    private static final long TIME_TO_RESET = 1000 * 60 * 10; //10 minutes

    @Autowired
    private SimpleDao simpleDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Account findByLogin(String login) {
        return simpleDao.findByField(Account.class, "login", login);
    }

    @Override
    public Account findAccWithPersonByLogin(String login) {
        Account account = simpleDao.findByField(Account.class, "login", login);;

        String sqlQuery = String.format("SELECT person_ref FROM account WHERE account_id = %s ", account.getId());
        Long Id = ((BigInteger)simpleDao.execQuery(sqlQuery).get(0)).longValue();
        Person person = findPersonById(Id);
        account.setPerson(person);
        return account;
    }


    @Override
    @Transactional
    public int addAttemptLogIn(String login) {
        Account account = simpleDao.findByField(Account.class, "login", login);
        Date currentDateTime = Calendar.getInstance().getTime();
        Date dateTime = account.getLoginTime();
        if(dateTime != null && currentDateTime.getTime() - dateTime.getTime() < TIME_TO_RESET){
            account.setTimesLogged(account.getTimesLogged() + 1);
        } else {
            account.setLoginTime(currentDateTime);
            account.setTimesLogged(1);
        }
        simpleDao.saveOrUpdate(account);
        return account.getTimesLogged();
    }

    @Transactional(readOnly = true)
    @Override
    public boolean checkAllowedLogIn(String login, int times){
        Account account = simpleDao.findByField(Account.class, "login", login);
        Date currentDateTime = Calendar.getInstance().getTime();
        Date dateTime = account.getLoginTime();
        if(dateTime != null && currentDateTime.getTime() - dateTime.getTime() < TIME_TO_RESET && account.getTimesLogged() > times) {
            return false;
        }
        return true;
    }

    @Override
    public int getAttemptsLogIn(String login) {
        Account account = simpleDao.findByField(Account.class, "login", login);
        return account.getTimesLogged();
    }

    @Override
    @Transactional
    public Long saveAccount(Account account) {
        String query = String.format("INSERT INTO account (login, password, type, person_ref) " +
                "VALUES ('%s','%s','%s',%s) " +
                "RETURNING account_id", account.getLogin(), passwordEncoder.encode(account.getPassword()), account.getType(), account.getPerson().getId());
        List<BigInteger> lst =  simpleDao.execQuery(query);
        return lst.get(0).longValue();
    }

    @Override
    @Transactional
    public Long savePerson(Person person) {
        String query = String.format(
                "INSERT INTO person (first_name, second_name, patronymic, image_url, type) " +
                        "VALUES ('%s','%s','%s','%s','%s') " +
                        "RETURNING person_id", person.getFirstName(), person.getSecondName(), person.getPatronymic(),
                person.getImageUrl(), person instanceof Student ? "STUDENT" : "TEACHER");

        List<BigInteger> lst = simpleDao.execQuery(query);
        Long id = lst.get(0).longValue();
        if(person instanceof Teacher){
            Teacher teacher = (Teacher)person;
            simpleDao.execSqlUpdateQuery(String.format("INSERT INTO teacher (teacher_id) VALUES (%s)", id));
            for (GenericSubject subject : teacher.getGenericSubjects()) {
                simpleDao.execSqlUpdateQuery(String.format("INSERT INTO teacher_generic_subject (teacher_ref, generic_subject_ref) VALUES (%s,%s)", id, subject.getId()));
            }
        } else {
            Student student = (Student)person;
            simpleDao.execSqlUpdateQuery(String.format("INSERT INTO student (student_id, academic_group_ref) VALUES (%s,%s)", id, student.getAcademicGroup().getId()));
        }
        return id;
    }

    @Override
    @Transactional(readOnly = true)
    public Person findPersonById(Long id) {
        String query = String.format("SELECT type FROM person WHERE person_id = %s LIMIT 1", id);
        Person person;
        String type = (String) simpleDao.execQuery(query).get(0);
        if(type.equals(Person.Type.STUDENT.getType())){
            person = simpleDao.findById(Student.class, id);
        } else {
            person = simpleDao.findById(Teacher.class, id);
        }
        return person;

    }

    @Override
    public Long findAccountIdByPersonId(Long id) {
        String query = String.format("SELECT account_ref FROM person WHERE person_id = %s LIMIT 1", id);
        Object queryResult = simpleDao.execQuery(query).get(0);
        Long res = queryResult == null ? null : ((BigInteger)queryResult).longValue();
        return res;
    }

    @Override
    public String getStudentDetails(Long id) {
        String query = String.format("SELECT t1.enter_year, t1.type, " +
                "t2.name " +
                "FROM academic_flow t1 JOIN academic_group t2 ON t1.academic_flow_id = t2.academic_flow_ref \n" +
                "JOIN student t3 ON t2.academic_group_id = t3.academic_group_ref\n" +
                "WHERE student_id = %s LIMIT 1", id);
        List<Object[]> result = simpleDao.execQuery(query);
        String enter_year = result.get(0)[0].toString();
        String type = result.get(0)[1].toString();
        String group_name = result.get(0)[2].toString();

        String ret = type + enter_year + ", group " + group_name;
        return ret;
    }
}
