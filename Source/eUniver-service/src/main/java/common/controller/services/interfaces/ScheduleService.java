package common.controller.services.interfaces;

import mobile.model.ClassSchedule;

import java.util.Date;

/**
 * Created by ПАХАРЬ on 13.11.2016.
 */
public interface ScheduleService {

    ClassSchedule getSchedule(Date from, Date to, String username);

}
