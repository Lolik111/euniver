package university.common;

import common.controller.domain.IdentifiedEntity;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * Created by Damir on 04.07.16.
 */
public interface SimpleDao {

    void configure(SessionFactory sessionFactory);

    <D> D findById(Class<D> aClass, Serializable aId);

    <D> List<D> fetchAll(Class<D> aClass);

    <D> D findByField(Class<D> aClass, String aFieldName, Object value);


    /**
     * Создаёт сущность, если её нет в бд, в противном случае
     * @param aEntity
     */
    <E extends IdentifiedEntity> void saveOrUpdate(E aEntity);

    void execSqlUpdateQuery(String query);

    <E> List<E> execQuery(String query);

    List<Map<String,Object>> execQueryMapResult(String query);

    Map<String, Object> execQueryMapSingleResult(String queryStr);
}
