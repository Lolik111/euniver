package university.common;

import common.controller.domain.IdentifiedEntity;
import org.hibernate.*;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Table;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.hibernate.transform.ResultTransformer;
import org.hibernate.transform.Transformers;
import org.hibernate.type.Type;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Attr;
import university.domain.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.lang.Class;
import java.lang.reflect.Executable;
import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by Damir on 04.07.16.
 */
@Repository("SimpleDao")
public class SimpleDaoImpl extends HibernateDaoSupport implements SimpleDao {

    @Resource(name = "sessionFactory")
    @Override
    public void configure(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    @Transactional(readOnly = true)
    public <D> List<D> fetchAll(final Class<D> aClass) {
        String query = "SELECT * FROM " + getTableName(aClass);
        if (aClass.getSuperclass().equals(Person.class)) {
            query = "SELECT * FROM " + getTableName(aClass) + " JOIN Person on " + aClass.getAnnotation(AttributeOverride.class).column().name() + " = person_id";
        }
        return getSessionFactory().getCurrentSession().createSQLQuery(query).addEntity(aClass).list();
    }

    @Override
    @Transactional(readOnly = true)
    public <D> D findById(Class<D> aClass, Serializable aId) {
        return getHibernateTemplate().execute(new HibernateCallback<D>() {
            @Override
            public D doInHibernate(Session aSession) throws HibernateException {
                String query = "SELECT * FROM " + getTableName(aClass) +  " WHERE " + getIdName(aClass) + " = ? LIMIT 1";
                if (aClass.getSuperclass().equals(Person.class)) {
                    query = "SELECT * FROM " + getTableName(aClass) + " JOIN Person on " + aClass.getAnnotation(AttributeOverride.class).column().name() + " = person_id"
                            +  " WHERE " + getIdName(aClass) + " = ? LIMIT 1";
                }

                return (D) aSession.createSQLQuery(query).addEntity( aClass).setParameter(0, aId).list().get(0);
            }
        });
    }

    @Override
    public <D> D findByField(Class<D> aClass, String aFieldName, Object aValue) {
        return getHibernateTemplate().execute(new HibernateCallback<D>() {
            @Override
            public D doInHibernate(Session aSession) throws HibernateException {
                    Query query = aSession.createSQLQuery("SELECT * FROM "+ getTableName(aClass) + " WHERE "+ getFieldName(aClass, aFieldName) + " = ? LIMIT 1").addEntity(aClass);
                    query.setParameter(0, aValue);
                    return (D) query.list().get(0);
            }
        });
    }


    @Override
    @Transactional
    public <E extends IdentifiedEntity> void saveOrUpdate(E aEntity) {
        getHibernateTemplate().saveOrUpdate(aEntity);
    }

    @Override
    public void execSqlUpdateQuery(String query) {
        getHibernateTemplate().execute(new HibernateCallback<Void>() {
            @Override
            public Void doInHibernate(Session aSession) throws HibernateException {
                aSession.createSQLQuery(query).executeUpdate();
                return null;
            }
        });
    }

    @Override
    public <E> List<E> execQuery(String query) {
        return getHibernateTemplate().execute(new HibernateCallback<List<E>>() {
            @Override
            public List<E> doInHibernate(Session session) throws HibernateException {
                return session.createSQLQuery(query).list();
            }
        });
    }

    @Override
    public List<Map<String, Object>> execQueryMapResult(String queryStr) {
        return getHibernateTemplate().execute(new HibernateCallback<List<Map<String, Object>>>() {
            @Override
            public List<Map<String, Object>> doInHibernate(Session session) throws HibernateException {
                Query query = session.createSQLQuery(queryStr);
                query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
                return query.list();
            }
        });
    }

    @Override
    public Map<String, Object> execQueryMapSingleResult(String queryStr) {
        return getHibernateTemplate().execute(new HibernateCallback<Map<String, Object>>() {
            @Override
            public Map<String, Object> doInHibernate(Session session) throws HibernateException {
                Query query = session.createSQLQuery(queryStr);
                query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
                return (Map<String, Object>) query.uniqueResult();
            }
        });
    }


    private <D> String getTableName(Class<D> dClass){
        Table an = dClass.getAnnotation(Table.class);
        String tableName = an.name();
        return tableName;
    }

    private <D> String getIdName(Class<D> dClass){
        AttributeOverride ao = dClass.getAnnotation(AttributeOverride.class);
        return ao.column().name();
    }

    private <D> String getFieldName(Class<D> dClass, String fieldName){
        Field field = null;
        try {
            field = dClass.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        javax.persistence.Column column = field.getAnnotation(Column.class);
        String baseFieldName = column.name();
        return baseFieldName;
    }

}
